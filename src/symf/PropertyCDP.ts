
import { PropertyCdp as PropertyCDPContract } from "./dependencies/typechain/PropertyCdp";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";

export class PropertyCDP {
  contract: PropertyCDPContract;
  firestore: firestore.Firestore;

  constructor(contract: PropertyCDPContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log("This is myCustom function and the address for the contract is ", this.contract.address);
  }
}
    