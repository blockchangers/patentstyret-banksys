// IMPORTS
import { ethers } from "ethers";
import Web3Modal, { IProviderOptions } from "web3modal";

// INTERFACES

// FUNCTIONS
export async function deployContract<T>(
  factory: new (signer?: ethers.Signer) => T
): Promise<T> {
  const provider = await getProvider().catch((_err) => {
    return getFallbackProvider();
  });
  return new factory(provider);
}

export async function getContract<T>(
  factory: (
    address: string,
    signerOrProvider: ethers.Signer | ethers.providers.Provider
  ) => T,
  address: string
): Promise<T> {
  const provider = await getProvider().catch((_err) => {
    return getFallbackProvider();
  });
  return factory(address, provider);
}

export async function handleError(error: any, handler: (err: string) => void) {
  if (error) {
    if (error.hasOwnProperty("message")) {
      handler(error.message);
    } else {
      handler(error);
    }
  } else {
    handler("Udefined error");
  }
  return undefined;
}

// FUNCTIONS PRIVATE
// brregFunctionPrivate

function getFallbackProvider(): ethers.Signer {
  const wallet = ethers.Wallet.fromMnemonic(
    "shrug antique orange tragic direct drop abstract ring carry price anchor train"
  );
  const provider = new ethers.providers.JsonRpcProvider({
    url: "https://u1txh1ent0-u1ieecy018-rpc.us1-azure.kaleido.io",
    user: "u1qdua80h5",
    password: "Er0LWdZuKqOza22YNQKhtdFCbqRzhzGCRhuZgrtHZ9s",
  });
  return wallet.connect(provider);
}

// brregFunctionPrivate

// devFunctionPrivate

// devFunctionPrivate

async function getProvider(): Promise<ethers.Signer> {
  try {
    if (localStorage.getItem("useFallbackProvider") === "true") {
      throw Error("Overide to fallback provider");
    }

    const web3ModalProvider = await getWeb3Provider();
    const web3provider = new ethers.providers.Web3Provider(web3ModalProvider);
    return web3provider.getSigner();
  } catch (error) {
    console.log("Could not get web3 provider from web3Modal");
    return await getFallbackProvider();
  }
}

async function getWeb3Provider(): Promise<any> {
  const providerOptions: IProviderOptions = {};
  const web3Modal = new Web3Modal({
    // network: "mainnet",
    cacheProvider: true,
    providerOptions, // required
  });
  return await web3Modal.connect();
}
