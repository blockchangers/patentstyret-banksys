// IMPORTS
import { initializeApp, firestore } from "firebase";

// FUNCTIONS
// brregFunction

var firebaseConfig = {
  apiKey: "AIzaSyDeKqc0QcK9VIU1VKJu93b9K4dfZyQFAlE",
  authDomain: "symf-test1.firebaseapp.com",
  databaseURL: "https://symf-test1.firebaseio.com",
  projectId: "symf-test1",
  storageBucket: "symf-test1.appspot.com",
  messagingSenderId: "258335690974",
  appId: "1:258335690974:web:349399746aede7f7dd324f",
};
// brregFunction

// devFunction

initializeApp(firebaseConfig);

// EXPORTS
export { firestore };
