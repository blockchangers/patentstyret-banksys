/* eslint-disable no-useless-rename */
// IMPORTS
// ERC20MintableBurnableImport

        import { ERC20MintableBurnable } from "./../ERC20MintableBurnable";
        import { Erc20MintableBurnableFactory as ERC20MintableBurnableFactory } from "./typechain/Erc20MintableBurnableFactory";
        
// ERC20MintableBurnableImport

// PropertyCDPImport

        import { PropertyCDP } from "./../PropertyCDP";
        import { PropertyCdpFactory as PropertyCDPFactory } from "./typechain/PropertyCdpFactory";
        
// PropertyCDPImport

// EntityRegistryImport

        import { EntityRegistry } from "./../EntityRegistry";
        import { EntityRegistryFactory as EntityRegistryFactory } from "./typechain/EntityRegistryFactory";
        
// EntityRegistryImport

// CapTableRegistryImport

        import { CapTableRegistry } from "./../CapTableRegistry";
        import { CapTableRegistryFactory as CapTableRegistryFactory } from "./typechain/CapTableRegistryFactory";
        
// CapTableRegistryImport

// CapTableQueImport

        import { CapTableQue } from "./../CapTableQue";
        import { CapTableQueFactory as CapTableQueFactory } from "./typechain/CapTableQueFactory";
        
// CapTableQueImport

// ERC1820RegistryDeployableImport

        import { ERC1820RegistryDeployable } from "./../ERC1820RegistryDeployable";
        import { Erc1820RegistryDeployableFactory as ERC1820RegistryDeployableFactory } from "./typechain/Erc1820RegistryDeployableFactory";
        
// ERC1820RegistryDeployableImport

// CapTableImport

        import { CapTable } from "./../CapTable";
        import { CapTableFactory as CapTableFactory } from "./typechain/CapTableFactory";
        
// CapTableImport

import { useState, useEffect } from "react";
import { getContract, handleError, deployContract } from "./Provider";

// FUNCTIONS
// ERC20MintableBurnableFunction

export function useERC20MintableBurnable(
  args: {
    address?: string;
  } = {}
): [ERC20MintableBurnable | undefined, boolean, string] {
  const [contract, setContract] = useState<ERC20MintableBurnable>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "ADDRESS_MUST_BE_PROVIDED";
    }
    getContract(ERC20MintableBurnableFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new ERC20MintableBurnable(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useERC20MintableBurnableFactory(): [
  ERC20MintableBurnableFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<ERC20MintableBurnableFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(ERC20MintableBurnableFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// ERC20MintableBurnableFunction

// PropertyCDPFunction

export function usePropertyCDP(
  args: {
    address?: string;
  } = {}
): [PropertyCDP | undefined, boolean, string] {
  const [contract, setContract] = useState<PropertyCDP>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "ADDRESS_MUST_BE_PROVIDED";
    }
    getContract(PropertyCDPFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new PropertyCDP(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function usePropertyCDPFactory(): [
  PropertyCDPFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<PropertyCDPFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(PropertyCDPFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// PropertyCDPFunction

// EntityRegistryFunction

export function useEntityRegistry(
  args: {
    address?: string;
  } = {}
): [EntityRegistry | undefined, boolean, string] {
  const [contract, setContract] = useState<EntityRegistry>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "0x7918B5b1D9faD841163FAc149fBd6EeA635AFc24";
    }
    getContract(EntityRegistryFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new EntityRegistry(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useEntityRegistryFactory(): [
  EntityRegistryFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<EntityRegistryFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(EntityRegistryFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// EntityRegistryFunction

// CapTableRegistryFunction

export function useCapTableRegistry(
  args: {
    address?: string;
  } = {}
): [CapTableRegistry | undefined, boolean, string] {
  const [contract, setContract] = useState<CapTableRegistry>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "0x5C15c5fAcDDe6e5c00c09A4aC167D83dA7d3A62A";
    }
    getContract(CapTableRegistryFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new CapTableRegistry(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useCapTableRegistryFactory(): [
  CapTableRegistryFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<CapTableRegistryFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(CapTableRegistryFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// CapTableRegistryFunction

// CapTableQueFunction

export function useCapTableQue(
  args: {
    address?: string;
  } = {}
): [CapTableQue | undefined, boolean, string] {
  const [contract, setContract] = useState<CapTableQue>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "0x8383C7298D224a10c24Aedfac86fE7474EbceB48";
    }
    getContract(CapTableQueFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new CapTableQue(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useCapTableQueFactory(): [
  CapTableQueFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<CapTableQueFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(CapTableQueFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// CapTableQueFunction

// ERC1820RegistryDeployableFunction

export function useERC1820RegistryDeployable(
  args: {
    address?: string;
  } = {}
): [ERC1820RegistryDeployable | undefined, boolean, string] {
  const [contract, setContract] = useState<ERC1820RegistryDeployable>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "0xE6656748aB0f79f96FC9F8829861794DB9e9C5e5";
    }
    getContract(ERC1820RegistryDeployableFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new ERC1820RegistryDeployable(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useERC1820RegistryDeployableFactory(): [
  ERC1820RegistryDeployableFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<ERC1820RegistryDeployableFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(ERC1820RegistryDeployableFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// ERC1820RegistryDeployableFunction

// CapTableFunction

export function useCapTable(
  args: {
    address?: string;
  } = {}
): [CapTable | undefined, boolean, string] {
  const [contract, setContract] = useState<CapTable>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    if (!args.address) {
      args.address = "ADDRESS_MUST_BE_PROVIDED";
    }
    getContract(CapTableFactory.connect, args.address)
      .catch((err) => handleError(err, setError))
      .then((_contract) => {
        setLoading(false);
        if (_contract) {
          setContract(new CapTable(_contract));
        } else {
          setContract(undefined);
        }
      });
  }, [args.address, error]);
  return [contract, loading, error];
}

// TODO : Find a way to infer parameters for deployment.
export function useCapTableFactory(): [
  CapTableFactory | undefined,
  boolean,
  string
] {
  const [factory, setFactory] = useState<CapTableFactory>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    deployContract(CapTableFactory)
      .catch((err) => handleError(err, setError))
      .then((_factory) => {
        setLoading(false);
        if (_factory) {
          setFactory(_factory);
        } else {
          setFactory(undefined);
        }
      });
  }, [error]);
  return [factory, loading, error];
}

      
// CapTableFunction
