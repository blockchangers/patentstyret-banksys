/* Generated by ts-generator ver. 0.0.8 */
/* tslint:disable */

import {
  ethers,
  EventFilter,
  Signer,
  BigNumber,
  BigNumberish,
  PopulatedTransaction
} from "ethers";
import {
  Contract,
  ContractTransaction,
  Overrides,
  CallOverrides
} from "@ethersproject/contracts";
import { BytesLike } from "@ethersproject/bytes";
import { Listener, Provider } from "@ethersproject/providers";
import { FunctionFragment, EventFragment, Result } from "@ethersproject/abi";

interface Erc1820RegistryInterface extends ethers.utils.Interface {
  functions: {
    "getInterfaceImplementer(address,bytes32)": FunctionFragment;
    "getManager(address)": FunctionFragment;
    "setInterfaceImplementer(address,bytes32,address)": FunctionFragment;
    "setManager(address,address)": FunctionFragment;
  };

  encodeFunctionData(
    functionFragment: "getInterfaceImplementer",
    values: [string, BytesLike]
  ): string;
  encodeFunctionData(functionFragment: "getManager", values: [string]): string;
  encodeFunctionData(
    functionFragment: "setInterfaceImplementer",
    values: [string, BytesLike, string]
  ): string;
  encodeFunctionData(
    functionFragment: "setManager",
    values: [string, string]
  ): string;

  decodeFunctionResult(
    functionFragment: "getInterfaceImplementer",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "getManager", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "setInterfaceImplementer",
    data: BytesLike
  ): Result;
  decodeFunctionResult(functionFragment: "setManager", data: BytesLike): Result;

  events: {};
}

export class Erc1820Registry extends Contract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  on(event: EventFilter | string, listener: Listener): this;
  once(event: EventFilter | string, listener: Listener): this;
  addListener(eventName: EventFilter | string, listener: Listener): this;
  removeAllListeners(eventName: EventFilter | string): this;
  removeListener(eventName: any, listener: Listener): this;

  interface: Erc1820RegistryInterface;

  functions: {
    getInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<{
      0: string;
    }>;

    "getInterfaceImplementer(address,bytes32)"(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<{
      0: string;
    }>;

    getManager(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<{
      0: string;
    }>;

    "getManager(address)"(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<{
      0: string;
    }>;

    setInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<ContractTransaction>;

    "setInterfaceImplementer(address,bytes32,address)"(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<ContractTransaction>;

    setManager(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<ContractTransaction>;

    "setManager(address,address)"(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<ContractTransaction>;
  };

  getInterfaceImplementer(
    _addr: string,
    _interfaceHash: BytesLike,
    overrides?: CallOverrides
  ): Promise<string>;

  "getInterfaceImplementer(address,bytes32)"(
    _addr: string,
    _interfaceHash: BytesLike,
    overrides?: CallOverrides
  ): Promise<string>;

  getManager(_addr: string, overrides?: CallOverrides): Promise<string>;

  "getManager(address)"(
    _addr: string,
    overrides?: CallOverrides
  ): Promise<string>;

  setInterfaceImplementer(
    _addr: string,
    _interfaceHash: BytesLike,
    _implementer: string,
    overrides?: Overrides
  ): Promise<ContractTransaction>;

  "setInterfaceImplementer(address,bytes32,address)"(
    _addr: string,
    _interfaceHash: BytesLike,
    _implementer: string,
    overrides?: Overrides
  ): Promise<ContractTransaction>;

  setManager(
    _addr: string,
    _newManager: string,
    overrides?: Overrides
  ): Promise<ContractTransaction>;

  "setManager(address,address)"(
    _addr: string,
    _newManager: string,
    overrides?: Overrides
  ): Promise<ContractTransaction>;

  callStatic: {
    getInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<string>;

    "getInterfaceImplementer(address,bytes32)"(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<string>;

    getManager(_addr: string, overrides?: CallOverrides): Promise<string>;

    "getManager(address)"(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<string>;

    setInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: CallOverrides
    ): Promise<void>;

    "setInterfaceImplementer(address,bytes32,address)"(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: CallOverrides
    ): Promise<void>;

    setManager(
      _addr: string,
      _newManager: string,
      overrides?: CallOverrides
    ): Promise<void>;

    "setManager(address,address)"(
      _addr: string,
      _newManager: string,
      overrides?: CallOverrides
    ): Promise<void>;
  };

  filters: {};

  estimateGas: {
    getInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    "getInterfaceImplementer(address,bytes32)"(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    getManager(_addr: string, overrides?: CallOverrides): Promise<BigNumber>;

    "getManager(address)"(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    setInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<BigNumber>;

    "setInterfaceImplementer(address,bytes32,address)"(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<BigNumber>;

    setManager(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<BigNumber>;

    "setManager(address,address)"(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<BigNumber>;
  };

  populateTransaction: {
    getInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    "getInterfaceImplementer(address,bytes32)"(
      _addr: string,
      _interfaceHash: BytesLike,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    getManager(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    "getManager(address)"(
      _addr: string,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    setInterfaceImplementer(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<PopulatedTransaction>;

    "setInterfaceImplementer(address,bytes32,address)"(
      _addr: string,
      _interfaceHash: BytesLike,
      _implementer: string,
      overrides?: Overrides
    ): Promise<PopulatedTransaction>;

    setManager(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<PopulatedTransaction>;

    "setManager(address,address)"(
      _addr: string,
      _newManager: string,
      overrides?: Overrides
    ): Promise<PopulatedTransaction>;
  };
}
