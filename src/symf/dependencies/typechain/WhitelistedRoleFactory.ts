/* Generated by ts-generator ver. 0.0.8 */
/* tslint:disable */

import { Signer } from "ethers";
import { Provider, TransactionRequest } from "@ethersproject/providers";
import { Contract, ContractFactory, Overrides } from "@ethersproject/contracts";

import { WhitelistedRole } from "./WhitelistedRole";

export class WhitelistedRoleFactory extends ContractFactory {
  constructor(signer?: Signer) {
    super(_abi, _bytecode, signer);
  }

  deploy(overrides?: Overrides): Promise<WhitelistedRole> {
    return super.deploy(overrides || {}) as Promise<WhitelistedRole>;
  }
  getDeployTransaction(overrides?: Overrides): TransactionRequest {
    return super.getDeployTransaction(overrides || {});
  }
  attach(address: string): WhitelistedRole {
    return super.attach(address) as WhitelistedRole;
  }
  connect(signer: Signer): WhitelistedRoleFactory {
    return super.connect(signer) as WhitelistedRoleFactory;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): WhitelistedRole {
    return new Contract(address, _abi, signerOrProvider) as WhitelistedRole;
  }
}

const _abi = [
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "WhitelistAdminAdded",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "WhitelistAdminRemoved",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "WhitelistedAdded",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "WhitelistedRemoved",
    type: "event"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "addWhitelistAdmin",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "addWhitelisted",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: true,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "isWhitelistAdmin",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: true,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "isWhitelisted",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "removeWhitelisted",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: false,
    inputs: [],
    name: "renounceWhitelistAdmin",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: false,
    inputs: [],
    name: "renounceWhitelisted",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  }
];

const _bytecode =
  "0x60806040526100266100186001600160e01b0361002b16565b6001600160e01b0361002f16565b61016f565b3390565b61004781600061007e60201b6104231790919060201c565b6040516001600160a01b038216907f22380c05984257a1cb900161c713dd71d39e74820f1aea43bd3f1bdd2096129990600090a250565b61009182826001600160e01b0361010816565b156100e3576040805162461bcd60e51b815260206004820152601f60248201527f526f6c65733a206163636f756e7420616c72656164792068617320726f6c6500604482015290519081900360640190fd5b6001600160a01b0316600090815260209190915260409020805460ff19166001179055565b60006001600160a01b03821661014f5760405162461bcd60e51b81526004018080602001828103825260228152602001806107416022913960400191505060405180910390fd5b506001600160a01b03166000908152602091909152604090205460ff1690565b6105c38061017e6000396000f3fe608060405234801561001057600080fd5b506004361061006d5760003560e01c806310154bad14610072578063291d95491461009a5780633af32abf146100c05780634c5a628c146100fa5780637362d9c814610102578063bb5f747b14610128578063d6cd94731461014e575b600080fd5b6100986004803603602081101561008857600080fd5b50356001600160a01b0316610156565b005b610098600480360360208110156100b057600080fd5b50356001600160a01b03166101ad565b6100e6600480360360208110156100d657600080fd5b50356001600160a01b03166101fc565b604080519115158252519081900360200190f35b610098610215565b6100986004803603602081101561011857600080fd5b50356001600160a01b0316610227565b6100e66004803603602081101561013e57600080fd5b50356001600160a01b0316610276565b610098610288565b610166610161610298565b610276565b6101a15760405162461bcd60e51b815260040180806020018281038252604081526020018061054f6040913960400191505060405180910390fd5b6101aa8161029c565b50565b6101b8610161610298565b6101f35760405162461bcd60e51b815260040180806020018281038252604081526020018061054f6040913960400191505060405180910390fd5b6101aa816102e4565b600061020f60018363ffffffff61032c16565b92915050565b610225610220610298565b610393565b565b610232610161610298565b61026d5760405162461bcd60e51b815260040180806020018281038252604081526020018061054f6040913960400191505060405180910390fd5b6101aa816103db565b600061020f818363ffffffff61032c16565b610225610293610298565b6102e4565b3390565b6102ad60018263ffffffff61042316565b6040516001600160a01b038216907fee1504a83b6d4a361f4c1dc78ab59bfa30d6a3b6612c403e86bb01ef2984295f90600090a250565b6102f560018263ffffffff6104a416565b6040516001600160a01b038216907f270d9b30cf5b0793bbfd54c9d5b94aeb49462b8148399000265144a8722da6b690600090a250565b60006001600160a01b0382166103735760405162461bcd60e51b815260040180806020018281038252602281526020018061052d6022913960400191505060405180910390fd5b506001600160a01b03166000908152602091909152604090205460ff1690565b6103a460008263ffffffff6104a416565b6040516001600160a01b038216907f0a8eb35e5ca14b3d6f28e4abf2f128dbab231a58b56e89beb5d636115001e16590600090a250565b6103ec60008263ffffffff61042316565b6040516001600160a01b038216907f22380c05984257a1cb900161c713dd71d39e74820f1aea43bd3f1bdd2096129990600090a250565b61042d828261032c565b1561047f576040805162461bcd60e51b815260206004820152601f60248201527f526f6c65733a206163636f756e7420616c72656164792068617320726f6c6500604482015290519081900360640190fd5b6001600160a01b0316600090815260209190915260409020805460ff19166001179055565b6104ae828261032c565b6104e95760405162461bcd60e51b815260040180806020018281038252602181526020018061050c6021913960400191505060405180910390fd5b6001600160a01b0316600090815260209190915260409020805460ff1916905556fe526f6c65733a206163636f756e7420646f6573206e6f74206861766520726f6c65526f6c65733a206163636f756e7420697320746865207a65726f206164647265737357686974656c69737441646d696e526f6c653a2063616c6c657220646f6573206e6f742068617665207468652057686974656c69737441646d696e20726f6c65a265627a7a72315820358680536d98d0818c19d38d48bfe994fff23f503ec621a008e946ba6429618864736f6c634300050f0032526f6c65733a206163636f756e7420697320746865207a65726f2061646472657373";
