/* Generated by ts-generator ver. 0.0.8 */
/* tslint:disable */

import { Contract, Signer } from "ethers";
import { Provider } from "@ethersproject/providers";

import { ControllerRole } from "./ControllerRole";

export class ControllerRoleFactory {
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): ControllerRole {
    return new Contract(address, _abi, signerOrProvider) as ControllerRole;
  }
}

const _abi = [
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "controllerAdded",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "controllerRemoved",
    type: "event"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "newController",
        type: "address"
      }
    ],
    name: "addController",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: true,
    inputs: [],
    name: "isController",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "controllerToRemove",
        type: "address"
      }
    ],
    name: "removeController",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  }
];
