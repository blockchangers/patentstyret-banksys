/* Generated by ts-generator ver. 0.0.8 */
/* tslint:disable */

import { Signer } from "ethers";
import { Provider, TransactionRequest } from "@ethersproject/providers";
import { Contract, ContractFactory, Overrides } from "@ethersproject/contracts";

import { BlacklistedRole } from "./BlacklistedRole";

export class BlacklistedRoleFactory extends ContractFactory {
  constructor(signer?: Signer) {
    super(_abi, _bytecode, signer);
  }

  deploy(overrides?: Overrides): Promise<BlacklistedRole> {
    return super.deploy(overrides || {}) as Promise<BlacklistedRole>;
  }
  getDeployTransaction(overrides?: Overrides): TransactionRequest {
    return super.getDeployTransaction(overrides || {});
  }
  attach(address: string): BlacklistedRole {
    return super.attach(address) as BlacklistedRole;
  }
  connect(signer: Signer): BlacklistedRoleFactory {
    return super.connect(signer) as BlacklistedRoleFactory;
  }
  static connect(
    address: string,
    signerOrProvider: Signer | Provider
  ): BlacklistedRole {
    return new Contract(address, _abi, signerOrProvider) as BlacklistedRole;
  }
}

const _abi = [
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "BlacklistAdminAdded",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "BlacklistAdminRemoved",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "BlacklistedAdded",
    type: "event"
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "BlacklistedRemoved",
    type: "event"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "addBlacklistAdmin",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "addBlacklisted",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: true,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "isBlacklistAdmin",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: true,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "isBlacklisted",
    outputs: [
      {
        internalType: "bool",
        name: "",
        type: "bool"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address"
      }
    ],
    name: "removeBlacklisted",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    constant: false,
    inputs: [],
    name: "renounceBlacklistAdmin",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  }
];

const _bytecode =
  "0x6080604052610016336001600160e01b0361001b16565b61015b565b61003381600061006a60201b6103541790919060201c565b6040516001600160a01b038216907fa6124c7f565d239231ddc9de42e684db7443c994c658117542be9c50f561943890600090a250565b61007d82826001600160e01b036100f416565b156100cf576040805162461bcd60e51b815260206004820152601f60248201527f526f6c65733a206163636f756e7420616c72656164792068617320726f6c6500604482015290519081900360640190fd5b6001600160a01b0316600090815260209190915260409020805460ff19166001179055565b60006001600160a01b03821661013b5760405162461bcd60e51b815260040180806020018281038252602281526020018061061e6022913960400191505060405180910390fd5b506001600160a01b03166000908152602091909152604090205460ff1690565b6104b48061016a6000396000f3fe608060405234801561001057600080fd5b50600436106100625760003560e01c806316d2e65014610067578063188efc16146100a1578063243f2473146100c9578063c6a276c2146100d1578063d3ce7905146100f7578063fe575a871461011d575b600080fd5b61008d6004803603602081101561007d57600080fd5b50356001600160a01b0316610143565b604080519115158252519081900360200190f35b6100c7600480360360208110156100b757600080fd5b50356001600160a01b031661015b565b005b6100c7610179565b6100c7600480360360208110156100e757600080fd5b50356001600160a01b0316610184565b6100c76004803603602081101561010d57600080fd5b50356001600160a01b031661019f565b61008d6004803603602081101561013357600080fd5b50356001600160a01b03166101ba565b6000610155818363ffffffff6101cd16565b92915050565b61016433610143565b61016d57600080fd5b61017681610234565b50565b6101823361027c565b565b61018d33610143565b61019657600080fd5b610176816102c4565b6101a833610143565b6101b157600080fd5b6101768161030c565b600061015560018363ffffffff6101cd16565b60006001600160a01b0382166102145760405162461bcd60e51b815260040180806020018281038252602281526020018061045e6022913960400191505060405180910390fd5b506001600160a01b03166000908152602091909152604090205460ff1690565b61024560018263ffffffff61035416565b6040516001600160a01b038216907fdbe320accb74107e8da655fa6a1e2b454c3102a3985d4201aba99308881a410a90600090a250565b61028d60008263ffffffff6103d516565b6040516001600160a01b038216907fba73eacdfe215f630abb6a8a78e5be613e50918b52e691bba35d46c06e20d6c890600090a250565b6102d560018263ffffffff6103d516565b6040516001600160a01b038216907ff38e60871ec534937251cd91cad807e15f55f1f6815128faecc256e71994b49790600090a250565b61031d60008263ffffffff61035416565b6040516001600160a01b038216907fa6124c7f565d239231ddc9de42e684db7443c994c658117542be9c50f561943890600090a250565b61035e82826101cd565b156103b0576040805162461bcd60e51b815260206004820152601f60248201527f526f6c65733a206163636f756e7420616c72656164792068617320726f6c6500604482015290519081900360640190fd5b6001600160a01b0316600090815260209190915260409020805460ff19166001179055565b6103df82826101cd565b61041a5760405162461bcd60e51b815260040180806020018281038252602181526020018061043d6021913960400191505060405180910390fd5b6001600160a01b0316600090815260209190915260409020805460ff1916905556fe526f6c65733a206163636f756e7420646f6573206e6f74206861766520726f6c65526f6c65733a206163636f756e7420697320746865207a65726f2061646472657373a265627a7a72315820d60c166b456bc29f7a083ea056537c4344f62179fec338de8bace29e323c0df064736f6c634300050f0032526f6c65733a206163636f756e7420697320746865207a65726f2061646472657373";
