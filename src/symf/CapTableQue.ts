
import { CapTableQue as CapTableQueContract } from "./dependencies/typechain/CapTableQue";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";

export class CapTableQue {
  contract: CapTableQueContract;
  firestore: firestore.Firestore;

  constructor(contract: CapTableQueContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log("This is myCustom function and the address for the contract is ", this.contract.address);
  }
}
    