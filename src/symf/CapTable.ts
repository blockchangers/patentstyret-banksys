import { CapTable as CapTableContract } from "./dependencies/typechain/CapTable";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";

export interface RightData {
  // regNr: number | string;
  // lapseDate: string;
  // limitations: string;
  // remarks: string;
  // regStatus: string;
  // validFrom: string;
}
export interface CDPData {
  regNr: number | string;
  lapseDate: string;
  limitations: string;
  remarks: string;
  regStatus: string;
  validFrom: string;
  docId?: string;
  txData?: TxData;
  rightAddress: string;
  cdpAddress: string;
}

export interface Holder {
  name: string;
  address: string;
}

export interface TxData {
  pledgor: Holder;
  pledgee: Holder;
  totalValue: number;
  yesno: boolean;
  remarks: string;
  limitations: string;
  validFrom: string;
}

export class CapTable {
  contract: CapTableContract;
  firestore: firestore.Firestore;

  constructor(contract: CapTableContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log(
      "This is myCustom function and the address for the contract is ",
      this.contract.address
    );
  }

  async name() {
    return await this.contract.name();
  }

  async updateData(data: RightData) {
    return this.firestore
      .collection("rights")
      .doc(this.contract.address)
      .set(data, { merge: true });
  }

  async getData(): Promise<RightData | undefined> {
    const doc = await this.firestore
      .collection("rights")
      .doc(this.contract.address)
      .get();
    if (doc.exists) {
      return doc.data() as RightData;
    }
    return undefined;
  }

  async getCDPdata(cdpAddress: string): Promise<CDPData | undefined> {
    const doc = await this.firestore
      .collection("cdp")
      .doc(cdpAddress)
      .get();
    if (doc.exists) {
      return doc.data() as CDPData;
    }
    return undefined;
  }

  async updateCDPData(cdpAddress: string, data: CDPData) {
    return this.firestore
      .collection("cdp")
      .doc(cdpAddress)
      .set(data, { merge: true });
  }
}
