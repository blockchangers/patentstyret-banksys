import { EntityRegistry as EntityRegistryContract } from "./dependencies/typechain/EntityRegistry";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";
import { ethers } from "ethers";

export class EntityRegistry {
  contract: EntityRegistryContract;
  firestore: firestore.Firestore;

  constructor(contract: EntityRegistryContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log(
      "This is myCustom function and the address for the contract is ",
      this.contract.address
    );
  }

  async suggest(id: string, name: string) {
    const inputName = unescape(encodeURIComponent(name))
      .substr(0, 31)
      .replace(/[\u{0080}-\u{FFFF}]/gu, "");
    return this.contract.suggest_id_to_address(
      ethers.utils.formatBytes32String(id.substr(0, 31)),
      ethers.utils.formatBytes32String(inputName)
    );
  }
  async set(id: string, name: string, address: string) {
    const inputName = unescape(encodeURIComponent(name))
      .substr(0, 31)
      .replace(/[\u{0080}-\u{FFFF}]/gu, "");
    return this.contract.set_id_to_address(
      ethers.utils.formatBytes32String(id.substr(0, 31)),
      ethers.utils.formatBytes32String(inputName),
      address
    );
  }

  async newId(evenFilter: ethers.EventFilter) {
    this.contract.on(evenFilter, (e) => {
      console.log("Some event", e);
    });
    //
  }

  async newAddressForId(id: string): Promise<string> {
    const filter = this.contract.filters.newId(
      null,
      ethers.utils.formatBytes32String(id.substr(0, 31)),
      null
    );
    return new Promise((resolve) => {
      this.contract.on(filter, (address) => {
        this.contract.removeAllListeners(filter);
        return resolve(address);
      });
    });
  }

  async addressForName(name: string): Promise<string> {
    const input = unescape(encodeURIComponent(name))
      .substr(0, 31)
      .replace(/[\u{0080}-\u{FFFF}]/gu, "");
    console.log("input", input);
    return await this.contract.get_address_from_name(
      ethers.utils.formatBytes32String(input)
    );
  }

  async nameForAddress(adr: string): Promise<string> {
    return ethers.utils.parseBytes32String(
      await this.contract.get_name_from_address(adr)
    );
  }
}
