
import { Erc20MintableBurnable as ERC20MintableBurnableContract } from "./dependencies/typechain/Erc20MintableBurnable";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";

export class ERC20MintableBurnable {
  contract: ERC20MintableBurnableContract;
  firestore: firestore.Firestore;

  constructor(contract: ERC20MintableBurnableContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log("This is myCustom function and the address for the contract is ", this.contract.address);
  }
}
    