
import { Erc1820RegistryDeployable as ERC1820RegistryDeployableContract } from "./dependencies/typechain/Erc1820RegistryDeployable";
import { firestore as db } from "./dependencies/Firestore";
import { firestore } from "firebase";

export class ERC1820RegistryDeployable {
  contract: ERC1820RegistryDeployableContract;
  firestore: firestore.Firestore;

  constructor(contract: ERC1820RegistryDeployableContract) {
    this.contract = contract;
    this.firestore = db();
  }

  myCustomFunction() {
    console.log("This is myCustom function and the address for the contract is ", this.contract.address);
  }
}
    