/* eslint-disable react-hooks/exhaustive-deps */
import { BigNumber, ethers } from 'ethers';
import { Box, DataTable, Grid, RadioButtonGroup, Text, TextInput } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { SpinnerDiamond } from 'spinners-react';
import { useCapTableFactory, useCapTableRegistry } from '../../symf/Symf';
import { ShowImage } from '../ui/ShowImage';
import { Visibility } from '../ui/Visibility';


interface Props { }

export const RegistryList: React.FC<Props> = () => {
    const [capTableRegistry] = useCapTableRegistry()
    const [capTableFactory] = useCapTableFactory()
    const [data, setData] = useState<{ address: string, name: string, totalSupply: BigNumber }[]>([]);
    const history = useHistory();
    const [pageOffset, /* setListOffset */] = useState(0);
    const [pageLimit, setpageSize] = useState(10);
    const [listType, setListType] = useState("active");
    const [loading, setLoading] = useState(false);

    // useEffect(() => {
    //     if (capTableRegistry && capTableFactory) {
    //         updateList("active")
    //     }
    // }, [capTableRegistry, capTableFactory]);

    // Trigger update when pageOffset or pageLimit changes
    useEffect(() => {
        if (capTableRegistry && capTableFactory) {
            updateList(listType)
        }
    }, [capTableRegistry, capTableFactory, pageOffset, pageLimit, listType]);

    const updateList = async (type: "active" | "all" | string) => {
        // console.log("updateList to type: ", type);
        setListType(type)
        setLoading(true)
        setData([])
        if (capTableRegistry) {
            if (type === "active") {
                setList(Array.from(await capTableRegistry.contract.listActive().catch(() => [])))
            }
            else if (type === "all")
                setList(Array.from(await capTableRegistry.contract.list().catch(() => [])))
            else
                setList(Array.from([]))
        }
    }

    const setList = async (list: string[]) => {
        if (capTableFactory) {
            let addresses = await list.reverse().slice(pageOffset, Math.min(list.length, pageOffset + pageLimit)).filter((a, i) => list.indexOf(a) === i)
            addresses.forEach(address => capTableData(address))
            setTimeout(() => {
                setLoading(false)
            }, 1000)
        }
    }

    const capTableData = async (address: string) => {
        if (capTableFactory) {
            const capTable = capTableFactory.attach(address)
            const name = await capTable.name().catch(() => "No company found");
            // const symbol = await capTable.symbol().catch(() => "No symbol found");
            const totalSupplyBN = await capTable
                .totalSupply()
                .catch(() => ethers.constants.Zero);
            setData(old => [...old, { address, name, totalSupply: totalSupplyBN }])
        }
    };


    return (
        <Box align="center" gap="small">
            {/* <Heading level="2">Shadowregistry of rights</Heading> */}
            <Visibility visibilty="public" />
            <Box direction="column" gap="small">
                <RadioButtonGroup
                    direction="row"
                    name="doc"
                    options={[{
                        value: 'active',
                        label: 'Approved only'
                    }, {
                        value: 'all',
                        label: 'All'
                    }]}
                    value={listType}
                    onChange={(event: any) => setListType(event.target.value)}
                />
                <Grid columns={["xsmall", "auto"]}>
                    <Text alignSelf="center">Nr.</Text>
                    <TextInput size="small" type="number" value={pageLimit} onChange={e => setpageSize(parseInt(e.target.value))}></TextInput>
                </Grid>
            </Box>
            {data.length > 0
                ?
                <DataTable
                    onClickRow={(event: any) => {
                        history.push("/right/" + event.datum.address);
                    }}
                    columns={[
                        {
                            property: "address",
                            header: <Text>#</Text>,
                            render: (data) => (<Text>{data.address.substr(0, 4) + ".."}</Text>)
                        },
                        {
                            property: "image",
                            header: "",
                            render: (data) => {
                                return <ShowImage identifier={data.name} height="30px" width="30px"></ShowImage>
                            }
                        },
                        {
                            property: 'name',
                            header: <Text>Refrence Nr.</Text>,
                            "search": true,
                            render: (data) => (<Text>{data.name}</Text>)
                        },
                        // {
                        //     property: 'totalSupply',
                        //     header: <Text>Avtaler</Text>,
                        //     align: "center",
                        //     render: (data) => data.totalSupply.toString()

                        // },

                    ]}
                    data={data}
                />
                : (loading ?
                    <Box align="center">
                        <SpinnerDiamond color="brand"></SpinnerDiamond>
                        <Text>Fetching rights</Text>
                    </Box>
                    :
                    <Text>Empty list</Text>
                )
            }

        </Box>
    )
}