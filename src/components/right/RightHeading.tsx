import React, { useEffect, useState } from 'react';
import { Box, Heading } from 'grommet';
import { useCapTable } from './../../symf/Symf';
import { Visibility } from '../ui/Visibility';

interface Props {
    address: string
}

export const RightHeading: React.FC<Props> = ({ address }) => {

    const [capTable] = useCapTable({ address })
    const [name, setName] = useState("");

    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                const name = await capTable.name()
                setName(name)
            }
        };
        doAsync();
    }, [capTable])

    return (
        <Box>
            {name
                ? <Heading level="2">{"Right: " + name}</Heading>
                : <Heading>Right</Heading>
            }
            <Visibility visibilty="public" />

        </Box>
    )
}