import { ethers } from 'ethers';
import { Box, Grid, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { RightData } from '../../symf/CapTable';
import { ShowImage } from '../ui/ShowImage';
import { useCapTable } from './../../symf/Symf';
interface Props {
    address: string
}

export interface CapTableData {
    address: string
    name: string
    uuid: string
    totalSupply: string
}

export const RightInfo: React.FC<Props> = ({ address }) => {
    const [capTable] = useCapTable({ address })
    const [capTableData, setCapTableData] = useState<CapTableData>();
    const [rightdata, setRightData] = useState<RightData>();

    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                const name = await capTable.name().catch(() => "No company found");
                const totalSupplyBN = await capTable.contract.totalSupply()
                    .catch(() => ethers.constants.Zero);
                const totalSupply = totalSupplyBN.toString();
                const uuidBytes32 = await capTable.contract.getUuid()
                const uuid = ethers.utils.parseBytes32String(uuidBytes32)
                setCapTableData({ address, name, uuid, totalSupply });
            }
        };
        doAsync();
    }, [address, capTable])


    useEffect(() => {
        let subscribed = true;
        const doAsync = async () => {
            if (capTable) {
                let data = await capTable.getData()
                if (subscribed && data) {
                    setRightData(data)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [capTable])

    return (
        <Box gap="small">
            {/* {data &&
                <Grid columns={["small", "flex"]}>
                    <Text>Navn</Text>
                    <Text weight="bold">{data.name}</Text>
                </Grid>
            } */}
            {capTableData &&
                <Box gap="small">
                    <Grid columns={["small", "flex"]}>
                        <Text >Agreements</Text>
                        <Text weight="bold">{capTableData.totalSupply}</Text>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Image</Text>
                        <ShowImage identifier={capTableData.name} width="100px"></ShowImage>
                    </Grid>
                </Box>
            }
            {rightdata &&
                <Box gap="small">
                    {/* <Grid columns={["small", "flex"]}>
                        <Text >RegNr.</Text>
                        <Text weight="bold">{rightdata.regNr}</Text>
                    </Grid> */}

                </Box>
            }

        </Box>
    )
}