import { BigNumber, ethers } from 'ethers';
import { Box, DataTable, Heading, Text } from 'grommet';
import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { SpinnerDiamond } from 'spinners-react';
import { useCapTable } from '../../symf/Symf';
import { useCDPtools } from '../../utils/cdpHelpers';
import { Address2Name } from '../ui/Address2Name';
import { UIBox } from '../ui/UIBox';

interface Props {
    address: string,
    tokenHolderList: string[]
}

interface CollateralBalance {
    cdpAddress: string,
    address: string,
    partition: string
    amount: BigNumber
    collateralHolders?: string[]
    collateralAmounts?: BigNumber[]
}




export const CollateralBalances: React.FC<Props> = ({ address, tokenHolderList }) => {
    const history = useHistory()
    const [capTable] = useCapTable({ address })
    const [pageOffset, /* setTokenHoldersOffset */] = useState(0);
    const [pageLimit, /* setTokenHolderPageLimit */] = useState(10);
    const [partitions, setPartitions] = useState<string[]>([]);
    const { filterCDPholders, getColleralDetails } = useCDPtools();
    const [collateralBalances, setCollateralBalance] = useState<CollateralBalance[]>([]);


    // Get account and Partitions
    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                let partitionsBytes32 = await capTable.contract.totalPartitions()
                setPartitions(partitionsBytes32)
            }
        };
        doAsync();
    }, [capTable])

    const subscribed = useRef(true)
    const loading = useRef(false)
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && partitions.length > 0 && tokenHolderList.length > 0 && collateralBalances.length === 0 && !loading.current) {
                loading.current = true
                console.log("RUNNING collateral balances")
                // CONSIDER - If race errors make this for loop instead. 
                // let collateralBalanceHoldersArray = []
                // for (const tokenHolder of tokenHolderList) {
                const promises = tokenHolderList.reverse().slice(pageOffset, Math.min(tokenHolderList.length, pageOffset + pageLimit)).map(async tokenHolder => {
                    const tokenHolderByPartitionsPromises = await partitions.map(async (partitionBytes32) => {
                        const balance = await capTable.contract.balanceOfByPartition(partitionBytes32, tokenHolder)
                        return {
                            cdpAddress: tokenHolder,
                            address: tokenHolder,
                            amount: balance,
                            partition: partitionBytes32
                        }
                    })
                    const tokenHolderByPartitions = await Promise.all(tokenHolderByPartitionsPromises)
                    const cdpHolderAddresses = await filterCDPholders(tokenHolderByPartitions.map(tokenHolderByPartition => tokenHolderByPartition.address))

                    // FOR LATER IN CODE --- If tokenHolde is not CDP, we know it does not hold collateral so we can add it as a collateralBalanceHolder without collateral
                    const balanceTokenHolders = tokenHolderByPartitions.filter(tokenHolderByPartition => {
                        if (cdpHolderAddresses.indexOf(tokenHolderByPartition.address) === -1) {
                            return true
                        }
                        return false
                    })
                    const cdpTokenHolders = tokenHolderByPartitions.filter(tokenHolderByPartition => {
                        if (cdpHolderAddresses.indexOf(tokenHolderByPartition.address) !== -1) {
                            return true
                        }
                        return false
                    })

                    // Lets start getting collateral info
                    const collateralDetails = await getColleralDetails(cdpHolderAddresses).catch(err => {
                        console.log("error in getting colleralDetails ", err)
                        throw Error(err)
                    })
                    console.log("collateralDetails", collateralDetails);

                    const cdpTokenHoldersExpanded = cdpTokenHolders.map(cdpTokenHolder => {
                        return collateralDetails[cdpTokenHolder.address].cToken.map(cToken => {
                            return {
                                cdpAddress: cdpTokenHolder.address,
                                address: cToken.holder,
                                amount: cdpTokenHolder.amount,
                                partition: cdpTokenHolder.partition,
                                collateralHolders: collateralDetails[cdpTokenHolder.address].pToken.map(pToken => pToken.holder),
                                collateralAmounts: collateralDetails[cdpTokenHolder.address].pToken.map(pToken => pToken.balance),
                            }
                        })
                    }).flat()

                    // collateralBalanceHoldersArray.push()
                    return [...balanceTokenHolders, ...cdpTokenHoldersExpanded]
                }
                )
                const collateralBalanceHoldersArray = await Promise.all(promises)
                const collateralBalanceHolders = collateralBalanceHoldersArray.flat().map((v, i) => ({ ...v, id: i }))
                console.log("collateralBalanceHolders", collateralBalanceHolders);
                console.log("subscribed", subscribed)
                if (subscribed) {
                    setCollateralBalance(collateralBalanceHolders)
                }
            }
        };
        doAsync();
        return () => { subscribed.current = false }
    }, [capTable, collateralBalances.length, filterCDPholders, getColleralDetails, pageLimit, pageOffset, partitions, tokenHolderList])


    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'NOK',

        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });

    return (
        <Box gap="medium">
            <UIBox heading="Pledges">
                {collateralBalances.filter(s => s.collateralAmounts !== undefined).length > 0 ?
                    <DataTable
                        data={collateralBalances.filter(s => s.collateralAmounts !== undefined)}
                        onClickRow={(event: any) => {
                            history.push("/right/" + address + "/" + event.datum.cdpAddress);
                        }}
                        columns={[
                            // {
                            //     property: 'id',
                            //     header: "#",
                            //     primary: true,
                            //     render: () => ""
                            // },
                            {
                                property: 'address',
                                header: <Text>Pledge holder</Text>,
                                render: (data: CollateralBalance) => (
                                    <Box gap="small" pad="small" >
                                        {<Address2Name address={data.address}></Address2Name>}
                                    </Box>

                                )
                            },
                            // {
                            //     property: 'amount',
                            //     header: <Text>Antall</Text>,
                            //     render: (data: CollateralBalance) => (
                            //         <Box gap="small" pad="small" >
                            //             <Text truncate>{data.amount.toString()}</Text>
                            //         </Box>
                            //     )
                            // },
                            // {
                            //     property: 'partition',
                            //     header: <Text>Type</Text>,
                            //     render: (data: CollateralBalance) => (
                            //         <Box gap="small" pad="small" >
                            //             <Text truncate>{ethers.utils.parseBytes32String(data.partition)}</Text>
                            //         </Box>
                            //     )
                            // },
                            {
                                property: 'collateralHolders',
                                header: <Text>Owner</Text>,
                                render: (data: CollateralBalance) => (

                                    <Box gap="small" pad="small" >
                                        {data.collateralHolders &&
                                            data.collateralHolders.map((collateralHolder, i) => (
                                                <Box key={i}>
                                                    {<Address2Name address={collateralHolder}></Address2Name>}
                                                </Box>
                                            ))
                                        }
                                    </Box>
                                )
                            },
                            {
                                property: 'collateralAmounts',
                                header: <Text>Total value</Text>,
                                render: (data: CollateralBalance) => (
                                    <Box gap="small" pad="small" >
                                        {data.collateralAmounts &&
                                            <Text truncate>{data.collateralAmounts.map(bn => formatter.format(parseInt(bn.toString())))}</Text>
                                        }
                                    </Box>
                                )
                            }
                        ]}
                    ></DataTable>
                    : <Box align="center" >
                        {collateralBalances.length === 0 &&
                            <Box align="center" >
                                <SpinnerDiamond color="brand"></SpinnerDiamond>
                                <Heading level={4}>Fetching pledges...</Heading>
                            </Box>
                        }
                    </Box>
                }
            </UIBox>

            <UIBox heading="Ownership">
                {collateralBalances.filter(s => s.collateralAmounts === undefined && s.amount !== ethers.constants.Zero).length > 0 ?
                    <DataTable
                        data={collateralBalances.filter(s => s.collateralAmounts === undefined && s.amount !== ethers.constants.Zero)}
                        onClickRow={(event: any) => {
                            history.push("/right/" + address + "/" + event.datum.cdpAddress);
                        }}
                        columns={[
                            // {
                            //     property: 'id',
                            //     header: "#",
                            //     primary: true,
                            //     render: () => ""
                            // },
                            {
                                property: 'address',
                                header: <Text>{/* Eier */}</Text>,
                                render: (data: CollateralBalance) => (
                                    <Box gap="small" pad="small" >
                                        {<Address2Name address={data.address}></Address2Name>}
                                    </Box>

                                )
                            },
                            // {
                            //     property: 'amount',
                            //     header: <Text>Antall</Text>,
                            //     render: (data: CollateralBalance) => (
                            //         <Box gap="small" pad="small" >
                            //             <Text truncate>{data.amount.toString()}</Text>
                            //         </Box>
                            //     )
                            // },
                            // {
                            //     property: 'partition',
                            //     header: <Text>Type</Text>,
                            //     render: (data: CollateralBalance) => (
                            //         <Box gap="small" pad="small" >
                            //             <Text truncate>{ethers.utils.parseBytes32String(data.partition)}</Text>
                            //         </Box>
                            //     )
                            // },
                        ]}
                    ></DataTable>
                    : <Box align="center" >
                        {collateralBalances.length === 0 &&
                            <Box align="center" >
                                <SpinnerDiamond color="brand"></SpinnerDiamond>
                                <Heading level={4}>Fetching ownership...</Heading>
                            </Box>
                        }
                    </Box>
                }
            </UIBox>
        </Box>

    )
}