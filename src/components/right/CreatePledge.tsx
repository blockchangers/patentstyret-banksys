/* eslint-disable @typescript-eslint/no-unused-vars */
import { ethers } from 'ethers';
import { firestore, database } from 'firebase';
import { Box, Button, CheckBox, DateInput, Grid, Heading, Image, Select, Text, TextArea, TextInput } from 'grommet';
import { Add, Trash } from 'grommet-icons';
import React, { useCallback, useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { SpinnerDiamond } from 'spinners-react';
import { Holder } from '../../pages/CreateAgreementCDPPage';
import { useCapTableFactory, useCapTableRegistry, useEntityRegistry, useERC1820RegistryDeployable, usePropertyCDPFactory } from '../../symf/Symf';
import { getERC1400Addresses } from '../../utils/cdpHelpers';
import { addressToBytes32, NumToHexBytes32 } from '../../utils/dataBytes32Format';
import { UIBox } from '../ui/UIBox';
import { Visibility } from '../ui/Visibility';
import altinn from './../../assets/altinn.png';
import { DownloadDocumet } from '../ui/DownloadDocumet';
import { ShowImage } from '../ui/ShowImage';

interface Props {
    entities: string[],
    agreementId: string
}

export interface PledgeFormData {
    pledgor: string,
    pledgee: string
    totalValue: number,
    remarks: string,
    limitations: string,
    validFrom: string,
    rights: { name: string, address: string }[]
}

export const CreatePledge: React.FC<Props> = ({ entities, agreementId }) => {
    const history = useHistory();
    const { handleSubmit, control, formState, watch, getValues, reset } = useForm<PledgeFormData>({
        defaultValues: {
            pledgor: "",
            pledgee: "",
            totalValue: 0,
            remarks: "",
            limitations: "",
            validFrom: (new Date()).toISOString(),
            rights: []
        }
    })
    const { fields, append, prepend, remove, swap, move, insert } = useFieldArray({
        control, // control props comes from useForm (optional: if you are using FormContext)
        name: "rights", // unique name for your Field Array
        // keyName: "id", default to "id", you can change the key name
    });
    const [capTableFactory] = useCapTableFactory()
    const [propertyCDPFactory] = usePropertyCDPFactory()
    const [ERC1820] = useERC1820RegistryDeployable()
    const [document, setDocument] = useState<{ name: string, type: string, data: string | ArrayBuffer | null }>();
    const [documentId, setDocumentId] = useState<string>();
    const [entityRegistry] = useEntityRegistry()
    const [capTableRegistry] = useCapTableRegistry()
    const [currentHolder, setCurrentHolder] = useState<Holder>();

    const [newRightInput, setNewRightInput] = useState<string>("");
    const [addRightError, setAddRightError] = useState<string>("");
    const onDrop = useCallback(files => {
        for (const file of files) {
            console.log(file)
            const reader = new FileReader();
            // const rABS = !!reader.readAsBinaryString;
            reader.onload = (e) => {
                /* Parse data */
                if (e.target) {
                    if (typeof e.target.result === "string") {
                        console.log("Raw", e.target.result)
                        setDocument({
                            name: file.name,
                            type: file.type,
                            data: e.target.result
                        })
                    } else {
                        console.log("Error parsing document")
                    }
                }
            };
            reader.readAsDataURL(file)
            // if (rABS) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);
        }

    }, [])
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
    const [possiblePledgors, setPossiblePledgors] = useState<string[]>([]);
    const [completedRights, setCompletedRights] = useState<{ right: string, cdp: string }[]>([]);
    const [pledgeeRights, setPledgeeRights] = useState<string[]>([]);
    const [pledgorRights, setPledgorRights] = useState<string[]>([]);
    const [pledgeeRightsHasRun, setpledgeeRightsHasRun] = useState(false);
    const [pledgorRightsHasRun, setpledgorRightsHasRun] = useState(false);

    const watchAll = watch()
    const watchRights = watch("rights", [])
    const watchPledgee = watch("pledgee")
    const watchPledgor = watch("pledgor")
    const [loadingPledgorRights, setloadingPledgorRights] = useState(false);

    // useEffect(() => {
    //     console.log("watchAll", watchAll)
    // }, [watchAll])

    // useEffect(() => {
    //     let subscribed = true
    //     const doAsync = async () => {
    //         if (pledgeeRights.length === 0 && watchPledgee && watchPledgee !== "" && entityRegistry && capTableRegistry && capTableFactory) {
    //             console.log("Getting all rights for pledgee", watchPledgee)
    //             let pledgeeAddress = await entityRegistry.addressForName(watchPledgee)
    //             if (pledgeeAddress === ethers.constants.AddressZero) {
    //                 return
    //             }
    //             let list = await capTableRegistry.contract.listActive()
    //             let rights = []
    //             for (let i = 0; i < list.length; i++) {
    //                 let capTable = await capTableFactory.attach(list[i])
    //                 let addresses = await getERC1400Addresses(capTable, list[i])
    //                 if (addresses.indexOf(pledgeeAddress) !== -1) {
    //                     let rightName = await capTable.name()
    //                     rights.push(rightName)
    //                 }
    //             }
    //             console.log("Found pledgee rights", rights)
    //             if (subscribed) {
    //                 setPledgeeRights(rights)
    //                 setpledgeeRightsHasRun(true)
    //             }
    //         }
    //     };
    //     doAsync();
    //     return () => { subscribed = false }
    // }, [capTableFactory, capTableRegistry, entityRegistry, pledgeeRights, pledgeeRightsHasRun, watchPledgee])

    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (!loadingPledgorRights && !pledgeeRightsHasRun && watchPledgor && watchPledgor !== "" && entityRegistry && capTableRegistry && capTableFactory) {
                console.log("Getting all rights for pledgor", watchPledgor)
                if (subscribed) {
                    setloadingPledgorRights(true)
                } else {
                    return
                }
                let pledgorAddress = await entityRegistry.addressForName(watchPledgor)
                if (pledgorAddress === ethers.constants.AddressZero) {
                    return
                }
                let list = await capTableRegistry.contract.listActive()
                console.log("Will search through " + list.length + " rights")
                for (let i = 0; i < list.length; i++) {
                    let rights: string[] = []
                    console.log(i + "/" + list.length)
                    let capTable = await capTableFactory.attach(list[i])
                    let addresses = await getERC1400Addresses(capTable, list[i])
                    if (addresses.indexOf(pledgorAddress) !== -1) {
                        let rightName = await capTable.name()
                        rights.push(rightName)
                    }
                    addPledgorRight(rights)
                }
                setpledgorRightsHasRun(true)
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [capTableFactory, capTableRegistry, entityRegistry, loadingPledgorRights, pledgeeRightsHasRun, pledgorRights, pledgorRightsHasRun, watchPledgor])


    const addPledgorRight = (rights: string[]) => {
        setPledgorRights(old => [...old, ...rights])
    }

    const maybeAddPledgors = useCallback(async (rightAddress: string) => {
        if (capTableFactory && entityRegistry) {
            const capTable = await capTableFactory.attach(rightAddress)
            const addresses = await getERC1400Addresses(capTable, rightAddress)
            console.log("addresses", addresses)
            const namePromises = addresses.map(async adr => {
                return entityRegistry.nameForAddress(adr)
            })
            const names = await Promise.all(namePromises)
            if (true) {
                setPossiblePledgors(names)
            }
        }
    }, [capTableFactory, entityRegistry]);

    // Check if right has capTable, then add it to rights
    const maybeAddRight = useCallback(async (rightName: string) => {
        setAddRightError("")
        try {
            if (capTableFactory && capTableRegistry) {
                let result = {
                    found: false,
                    address: "0x"
                };
                let list = await capTableRegistry.contract.listActive()
                let promises = list.map(async adr => {
                    let capTable = await capTableFactory.attach(adr)
                    let name = await capTable.name()
                    if (name === rightName)
                        result = {
                            found: true,
                            address: adr
                        }
                })
                await Promise.all(promises)
                console.log("Done looping through every capTable, result ", result.found)
                if (result.found) {
                    console.log("Verified right")
                    prepend({ name: rightName, address: result.address, valid: true })
                    setNewRightInput("")
                    // maybeAddPledgors(result.address)
                }
            }
        } catch (error) {
            setAddRightError("Fant ikke rettighet.")
        }
    }, [capTableFactory, capTableRegistry, prepend])

    // Get currentHolder
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (entityRegistry) {
                let currentAddress = await entityRegistry.contract.signer.getAddress()
                let currentName = await entityRegistry.nameForAddress(currentAddress)
                if (subscribed) {
                    setCurrentHolder({
                        name: currentName,
                        address: currentAddress
                    })
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [entityRegistry])

    // Get current agreement and set data in form if exist.
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            let db = firestore()
            let docRef = await db.collection("agreement").doc(agreementId).get()
            if (docRef.exists && subscribed) {
                let data = docRef.data() as any
                if (data && subscribed) {
                    const static_data = { ...data, ...{ 'rights': [] } }
                    reset(static_data)
                    for (let i = 0; i < data.rights.length; i++) {
                        const right = data.rights[i]
                        maybeAddRight(right.name)
                    }
                    if (data.document) {
                        setDocumentId(data.document)
                    }

                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [agreementId, maybeAddRight, reset])


    const onSubmit = async (data: PledgeFormData) => {
        console.log("submit data", data);
        if (data.rights.length === 0) {
            return console.log("Rights cannot be 0")
        }

        if (capTableFactory && propertyCDPFactory && ERC1820 && entityRegistry && currentHolder) {
            try {
                console.log("addrss2", currentHolder)
                const pledgeeAddress = await entityRegistry.addressForName(data.pledgee)
                console.log(pledgeeAddress)
                const isPledgee = currentHolder.address === pledgeeAddress
                const pledgorAddress = await entityRegistry.addressForName(data.pledgor)
                const isPledgor = currentHolder.address === pledgorAddress
                const db = firestore()
                if (!isPledgor) {
                    console.log("Submitting as pledgee")
                    let docId = ""
                    if (document) {
                        const docRef = await db.collection("ducuments").add(document)
                        docId = docRef.id
                        setDocumentId(docId)
                    }

                    const firestoreData = {
                        ...data,
                        id: agreementId,
                        document: docId,
                        status: "Waiting pledgor"
                    }

                    return db
                        .collection("agreement")
                        .doc(agreementId)
                        .set(firestoreData, { merge: true });
                }

                if (isPledgor) {
                    console.log("Submitting as pledgor")
                    const amount = data.totalValue / data.rights.length
                    let completedRights: { right: string, cdp: string }[] = []
                    for (let i = 0; i < data.rights.length; i++) {
                        try {
                            const right = data.rights[i];
                            const propertyCDP = await propertyCDPFactory.deploy(ERC1820.contract.address);
                            console.log("Address cdp =", propertyCDP.address)
                            await propertyCDP.deployed()
                            // let CDPentity = await entityRegistry.set(ethers.utils.formatBytes32String(propertyCDP.address.substr(0, 31)), "CDP for right", propertyCDP.address)
                            // await CDPentity.wait();
                            const partition = ethers.utils.formatBytes32String("OWNERSHIP")
                            const capTable = await capTableFactory.attach(right.address)
                            const transferIssueByPartition = await capTable.transferByPartition(partition, propertyCDP.address, 1, "0x4341505f5441424c455f4344505f464c41470000000000000000000000000000" +
                                NumToHexBytes32(amount) +
                                addressToBytes32(pledgorAddress) +
                                addressToBytes32(pledgeeAddress))
                            await transferIssueByPartition.wait();
                            completedRights.push({ right: right.address, cdp: propertyCDP.address })

                        } catch (error) {
                            console.log("Error when transfer issuing cdp")
                        }
                    }
                    await db
                        .collection("agreement")
                        .doc(agreementId)
                        .set({
                            status: "Complete",
                            completed: completedRights
                        }, { merge: true })

                    setCompletedRights(completedRights)
                }
            } catch (error) {
                console.log(error)
            }
        }
    }

    const [searching, setSearching] = useState(false);
    const [searchQuery, setSerchQuery] = useState('');
    const [filteredEntities, setFilteredEntities] = useState<string[]>(entities);
    const [searchingPledgor, setSearchingPledgor] = useState(false);
    const [searchQueryPledgor, setSerchQueryPledgor] = useState('');
    const [filteredEntitiesPledgor, setFilteredEntitiesPledgor] = useState<string[]>(entities);

    useEffect(() => {
        const filteredEntities = entities.filter(
            s => s.toLowerCase().indexOf(searchQuery.toLowerCase()) >= 0,
        );
        setTimeout(() => {
            setSearching(false);
            setFilteredEntities(filteredEntities);
        }, 500);
    }, [searching, searchQuery, entities]);

    useEffect(() => {
        const filteredEntities = entities.filter(
            s => s.toLowerCase().indexOf(searchQueryPledgor.toLowerCase()) >= 0,
        );

        setTimeout(() => {
            setSearching(false);
            setFilteredEntitiesPledgor(filteredEntities);
        }, 500);
    }, [searchingPledgor, searchQueryPledgor, entities]);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Box margin="large" pad="small" gap="small">
                <Heading>New pledge agreement</Heading>
                <Visibility visibilty="context"></Visibility>
                <Grid columns={["1/2", "1/2"]} fill="horizontal" gap="small">

                    {/* Form */}
                    <UIBox heading="Information about the agreement">
                        <Grid columns={["small", "auto"]} gap="small" align="center">
                            <Text>Pledgee</Text>
                            <Controller
                                render={({ onChange, value, name }) => (
                                    <Select
                                        options={filteredEntities}
                                        labelKey={(holder) => holder}
                                        focusIndicator={false}
                                        multiple={false}
                                        value={value}
                                        onChange={(e) => onChange(e.option)}
                                        onSearch={query => {
                                            setSearching(true);
                                            setSerchQuery(query);
                                        }}
                                    ></Select>
                                )}
                                name="pledgee"
                                control={control}
                                rules={{ required: true }}
                            />

                        </Grid>

                        <Grid columns={["small", "auto"]} gap="small" align="center">
                            <Text>Pledgor / Rightsowner</Text>
                            <Controller
                                render={({ onChange, value, name }) => (
                                    <Select
                                        options={filteredEntitiesPledgor}
                                        labelKey={(holder) => holder}
                                        focusIndicator={false}
                                        multiple={false}
                                        value={value}
                                        // disabled={true}
                                        onChange={(e) => onChange(e.option)}
                                        onSearch={query => {
                                            setSearchingPledgor(true);
                                            setSerchQueryPledgor(query);
                                        }}
                                    ></Select>
                                )}
                                name="pledgor"
                                control={control}
                                rules={{ required: true }}
                            />
                        </Grid>

                        <Grid columns={["small", "auto"]} gap="small" align="center">
                            <Text>Total value for agreement</Text>
                            <Controller as={<TextInput />} name="totalValue" control={control} type="number" rules={{ required: false }} />
                        </Grid>

                        {/* <Grid columns={["small", "auto"]} gap="small" align="center">
                            <Text>Er dette ja/nei spørsmål?</Text>
                            <Controller as={<CheckBox />} name="yesno" control={control} rules={{ required: false }} />
                        </Grid> */}

                        <Grid columns={["auto"]} gap="small" align="center">
                            <Text>Remarks</Text>
                            <Controller as={<TextArea />} name="remarks" control={control} rules={{ required: false }} />
                        </Grid>

                        <Grid columns={["auto"]} gap="small" align="center">
                            <Text>Limitations</Text>
                            <Controller as={<TextArea />} name="limitations" control={control} rules={{ required: false }} />
                        </Grid>

                        <Grid columns={["auto"]} gap="small" align="center">
                            <Text>Valid from</Text>
                            <Controller
                                render={({ onChange, value, name }) => (
                                    <DateInput
                                        format="mm/dd/yyyy"
                                        value={value}
                                        onChange={(e: any) => {
                                            onChange(e.value)
                                        }}
                                    />
                                )}
                                name="validFrom" control={control} rules={{ required: false }} />
                        </Grid>

                        <Grid columns={["xsmall", "auto"]} gap="small" align="center">
                            <CheckBox></CheckBox>
                            <Text>
                                I declare that the contents of the uploaded agreement correspond to the fields I have registered on this website.
                            </Text>
                        </Grid>

                    </UIBox>

                    {/* Document */}
                    <Box gap="small">
                        <UIBox heading="Authentication">
                            <Grid columns={["small", "auto"]}>
                                <Text weight="bold">Logged in: </Text>
                                <Text>{currentHolder?.name}</Text>
                            </Grid>
                            <Grid columns={["small", "auto"]}>
                                <Text weight="bold">Authorization</Text>
                                <Text>- Create draft pledge agreement (need signature from pledgor)</Text>
                            </Grid>

                        </UIBox>
                        <UIBox heading="Reference to agreement">

                            <Box pad="small" gap="small">
                                <Box {...getRootProps()} background="white" >
                                    <input {...getInputProps()} />
                                    {
                                        isDragActive ?
                                            <p>Drop file here...</p> :
                                            <Image src={altinn} fit="contain" />
                                    }
                                </Box>
                                <Grid columns={["1/2", "auto"]} align="center" fill="horizontal">
                                    <Text>Status</Text>
                                    <Text weight="bold">{documentId === undefined ? "Not submitted" : "Submitted"}</Text>

                                </Grid>
                                {documentId &&
                                    <Grid columns={["2/3", "1/3"]} fill="horizontal">
                                        <Text></Text>
                                        <DownloadDocumet docId={documentId}></DownloadDocumet>
                                    </Grid>
                                }
                                <Text>After submission, the Norwegian Patent Office will assess the documentation.</Text>
                                <Text size="small">This will not be available online.</Text>
                            </Box>
                        </UIBox>
                    </Box>

                    {/* Rights */}
                    <UIBox heading="Rights included in this agreement" >
                        {fields.map((field, index) =>
                            <Grid key={field.id} columns={["small", "auto", "flex", "xsmall"]} align="start" gap="small">
                                <Text>Rights ID</Text>
                                <Controller as={<TextInput disabled={true} />} name={`rights[${index}].name`} control={control} rules={{ required: false }} defaultValue={field.name} />
                                <Grid columns={["xsmall", "auto"]} alignContent="start">
                                    <Text>Arrest: </Text>
                                    <Text>No</Text>
                                    {/* <Text>Eiere:</Text>
                                    <Text>{possiblePledgors.length}</Text> */}
                                </Grid>
                                <Button color="red" icon={<Trash></Trash>} onClick={() => remove(index)}></Button>
                                <Controller as={<TextInput type="hidden" />} name={`rights[${index}].address`} control={control} rules={{ required: false }} defaultValue={field.address} />
                            </Grid>
                        )}
                        {fields.length === 0 &&
                            <Text alignSelf="center" onClick={() => setNewRightInput("TM 201100639 (1062314)")}>No rights added</Text>
                        }
                        <Grid columns={["flex", "auto"]} gap="small">
                            <TextInput value={newRightInput} onChange={(e) => setNewRightInput(e.target.value)} placeholder="Register right ID"></TextInput>
                            <Button label="Add right" secondary icon={<Add></Add>}
                                onClick={() => maybeAddRight(newRightInput)}>
                            </Button>
                        </Grid>
                        <Text color="red" size="small">{addRightError}</Text>
                    </UIBox>

                    <UIBox heading="Pledgor rights">
                        <Grid columns={{ size: "flex", count: 3 }} gap="small" >

                            {pledgorRights.map((right, i) => (
                                <Button icon={<ShowImage identifier={right} height="20px" width="20px"></ShowImage>} key={i} label={right} onClick={() => maybeAddRight(right)}></Button>
                            ))}

                        </Grid>
                        {/* {loadingPledgorRights &&
                            <Box align="center">
                                <SpinnerDiamond color="brand"></SpinnerDiamond>
                            </Box>
                        } */}
                        {pledgorRights.length === 0 && !loadingPledgorRights &&
                            <Text size="small">Please register pledgor first.</Text>
                        }
                    </UIBox>
                    {/* 
                    <UIBox heading="Pledgee rights">
                        <Box direction="row-responsive">
                            {pledgeeRights.map((right, i) => (
                                <Text key={i}>{right}</Text>
                            ))}
                        </Box>
                    </UIBox> */}


                </Grid>


                <Box gap="small" margin={{ top: "large" }} align="end" fill="horizontal">
                    <Grid columns={["small", "auto"]} gap="small" align="center">
                        <Button type="submit" primary label="Submit" disabled={formState.isSubmitting} />
                    </Grid>
                </Box>
                <Box direction="row" gap="small" align="end">
                    {completedRights.map(({ right, cdp }, i) => (
                        <Button key={i} label={"Right details " + right} color="green" onClick={() => history.push("/right/" + right + "/" + cdp)}></Button>
                    ))}
                </Box>
            </Box>
        </form>
    )
}