import React from 'react';
import { Box, Heading, Button } from 'grommet';
import { useHistory, Link } from 'react-router-dom';
import { Account } from './ui/Account'

interface Props { }

export const Navigation: React.FC<Props> = () => {
    const history = useHistory();
    return (
        <>
            <Heading level="2" onClick={e => history.push("/")}>
                Rights management
              </Heading>
            <Box direction="row" gap="small">
                <Link to="/admin/db/upload">
                    <Button size="small" label="Upload DB" hoverIndicator focusIndicator={false} />
                </Link>
                {/* <Link to="/que">
                    <Button size="small" label="Que" hoverIndicator focusIndicator={false} />
                </Link> */}
                <Link to="/registry">
                    <Button size="small" label="Rights" hoverIndicator focusIndicator={false} />
                </Link>
                <Link to="/agreement/list">
                    <Button size="small" label="Agreements" hoverIndicator focusIndicator={false} />
                </Link>
                <Link to="/announcement/list">
                    <Button size="small" label="Announcements" hoverIndicator focusIndicator={false} />
                </Link>

                <Account></Account>
            </Box>
        </>
    )
}