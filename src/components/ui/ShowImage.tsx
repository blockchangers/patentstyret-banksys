import React, { useEffect, useState } from 'react';
import { Image, ImageProps } from 'grommet';
import ds_fallback from './../../assets/images/ds_fallback.jpg';
import pt_fallback from './../../assets/images/pt_fallback.jpg';
import tm_fallback from './../../assets/images/tm_fallback.jpg';

interface ShowImage extends ImageProps {
    identifier: string,
    height?: string,
    width?: string
}

export const ShowImage: React.FC<ShowImage> = ({ identifier, height, width, ...props }) => {
    const [currentImage, setCurrentImage] = useState(() => {
        // const rng = Math.floor(Math.random() * 3) + 1
        // console.log(rng)
        switch (identifier.substr(0, 2)) {
            case "DS":
                return ds_fallback
            case "TM":
                return tm_fallback;
            default:
                return pt_fallback;
        }
    });
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            try {
                // console.log(identifier.replace(/\s/g, '') + ".jpg")
                const _img = await import("./../../assets/images/rights/" + identifier.replace(/\s/g, '') + ".jpg"); // ./../../assets/images/rights/1-min.jpg
                if (subscribed) {
                    setCurrentImage(_img.default)
                }
            } catch (error) {
                // console.log("err")
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [identifier])
    return (
        <Image fill="vertical" fit="contain" src={currentImage} {...props} height={height} width={width} ></Image>
    )
}