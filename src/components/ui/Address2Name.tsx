import React, { useState, useEffect } from 'react';
import { Box, Text, Button } from 'grommet';
import Copy from "clipboard-copy";
import { Copy as CopyIcon } from 'grommet-icons';
import { useEntityRegistry } from '../../symf/Symf';
import { ethers } from 'ethers';

interface Props {
  address: string
  copy?: boolean
  size?: string
}

export const Address2Name: React.FC<Props> = ({ address, copy = true, size = "medium" }) => {
  const [color, setColor] = useState<string>("black");
  const [entityRegistry] = useEntityRegistry()

  const formatAddress = (address: string) => {
    return address.substr(0, 5) +
      ".." +
      address.substr(address.length - 2, address.length)
  }
  const [name, SetName] = useState(formatAddress(address));

  useEffect(() => {
    let subscribed = true
    const doAsync = async () => {
      if (entityRegistry) {
        try {
          let nameBytes32 = await entityRegistry.contract.get_name_from_address(address)
          if (subscribed) {
            SetName(ethers.utils.parseBytes32String(nameBytes32))
          }
        } catch (error) {
          console.log("Error when gettig name2address")
        }
      }

    };
    doAsync();
    return () => { subscribed = false }
  }, [address, entityRegistry, name])

  const handleCopy = () => {
    setColor("green")
    Copy(address)
    setTimeout(() => {
      setColor("text")
    }, 300)
  }

  return (
    <Box direction="row">
      <Text size={size}>{name}</Text>
      {copy &&
        <Button plain margin={{ left: "5px" }} alignSelf="center" icon={<CopyIcon size="15px" color={color} ></CopyIcon>} onClick={() => handleCopy()} hoverIndicator focusIndicator={false}></Button>
      }
    </Box>
  );
}