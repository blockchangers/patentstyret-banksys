import { Box, DropButton, Text } from 'grommet';
import { View } from 'grommet-icons';
import React, { useState } from 'react';

export enum VISIBILITY {
    PUBLIC,
    NIPO,
    CONTEXT
}

interface Props {
    visibilty?: string
}

export const Visibility: React.FC<Props> = ({ visibilty: _visibilty }) => {
    const [visibilty, /* setVisibilty */] = useState<VISIBILITY>(() => {
        if (!_visibilty) {
            return VISIBILITY.PUBLIC
        }
        if (_visibilty.toLowerCase() === "nipo") {
            return VISIBILITY.NIPO
        }
        if (_visibilty.toLowerCase() === "context") {
            return VISIBILITY.CONTEXT
        }
        return VISIBILITY.PUBLIC
    });
    const ICON = () => {
        if (visibilty === VISIBILITY.PUBLIC)
            return <View></View>
        if (visibilty === VISIBILITY.NIPO)
            return <View></View>
        if (visibilty === VISIBILITY.CONTEXT)
            return <View></View>
    }

    const TEXT = () => {
        if (visibilty === VISIBILITY.PUBLIC)
            return "Everyone can see this information"
        if (visibilty === VISIBILITY.NIPO)
            return "Only NIPO can see this information"
        if (visibilty === VISIBILITY.CONTEXT)
            return "NIPO and entities involved will see this information"
    }

    // const LABEL = () => {
    //     if (visibilty === VISIBILITY.PUBLIC)
    //         return "Public"
    //     if (visibilty === VISIBILITY.NIPO)
    //         return "NIPO"
    //     if (visibilty === VISIBILITY.CONTEXT)
    //         return "Context"
    // }

    return (
        <DropButton
            margin="medium"
            icon={ICON()}
            label={TEXT()}
            hoverIndicator={false}
            focusIndicator={false}
            plain
            size="small"
            dropAlign={{ bottom: 'top', right: "right" }}
            dropContent={
                <Box pad="small" background="white" >
                    <Text size="small">{TEXT()}</Text>
                </Box>
            }
        />
    )
}