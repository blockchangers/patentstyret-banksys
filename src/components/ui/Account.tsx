import { Box, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useEntityRegistry } from '../../symf/Symf';

interface Props { }

export const Account: React.FC<Props> = () => {
    const [name, setName] = useState("");
    const [entityRegistry] = useEntityRegistry()
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (entityRegistry) {
                let address = await entityRegistry.contract.signer.getAddress()
                let name = await entityRegistry.nameForAddress(address)
                if (name !== "" && subscribed) {
                    setName(name)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [entityRegistry])

    return (
        <Box align="center" alignContent="center" alignSelf="center">
            <Text size="small">User </Text>
            {name === "" ?
                <Text size="small" alignSelf="center">Ikke registrert</Text>
                :
                <Text weight="bold" size="small">{name}</Text>
            }
        </Box>
    )
}