import React, { useEffect, useState } from 'react';
import { Box, Button } from 'grommet';
import { firestore } from 'firebase';

interface Props {
    docId: string
}

export const DownloadDocumet: React.FC<Props> = ({ docId }) => {
    const [data, setData] = useState<any>(undefined);
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            let db = firestore()
            let docRef = await db.collection("ducuments").doc(docId).get()
            console.log(docId, docRef.exists)
            if (docRef.exists && subscribed) {
                const data = docRef.data()
                if (data) {
                    console.log("DAta", data)
                    setData(data)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [docId])

    return (
        <Box>
            {data &&
                <Button focusIndicator={false} size="small" label="Download agreement" target="_blank" href={data.data} /* onClick={() => downloadString(data.data, data.type, data.name)}  */ disabled={!data ? true : false}></Button>
            }
        </Box>
    )
}