/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useCallback, useState, useEffect, useRef } from 'react';
import { Box, Text } from 'grommet';
import { useDropzone } from 'react-dropzone'
import { read, utils } from 'xlsx'
import { useCapTableFactory, useCapTableQue, useERC1820RegistryDeployable, useEntityRegistry, usePropertyCDPFactory, useCapTableRegistry, useCapTable } from '../symf/Symf';
import { BigNumberish, BytesLike, Overrides, ethers } from 'ethers';
import { SpinnerDiamond } from "spinners-react";
import { NumToHexBytes32, addressToBytes32 } from '../utils/dataBytes32Format';
import { firestore } from "./../symf/dependencies/Firestore";

interface Props { }

interface Row {
    "Associated IP Rights": string
    "Created": string
    "Exclusive": string
    "GH -case if exists"?: string
    "Lapse date": string
    "Licensee/Pledgee": string
    "Licensor/Pledgor": string
    "Limitations"?: string
    "Reg Nr.": number
    "Reg.status": string
    "Remarks": string
    "Sign date"?: string
    "Type": "License" | "Pledge"
    "Valid from": string
    "ex": string
}

interface CapTableParams {
    name: string,
    symbol: string,
    granularity: BigNumberish,
    controllers: string[],
    certificateSigner: string,
    certificateActivated: boolean,
    defaultPartitions: BytesLike[],
    capTableQue: string,
    erc1820: string,
    uuid: BytesLike,
    overrides?: Overrides | undefined
}

export const UploadDB: React.FC<Props> = () => {
    const [data, setData] = useState<Row[]>([]);
    const onDrop = useCallback(files => {
        setMessage("Reading file")
        for (const file of files) {
            console.log(file)
            setMessage("Parsing file")
            const reader = new FileReader();
            const rABS = !!reader.readAsBinaryString;
            reader.onload = (e) => {
                /* Parse data */
                if (e.target) {
                    const target = e.target.result;
                    const wb = read(target, { type: rABS ? 'binary' : 'array' });
                    /* Get first worksheet */
                    const wsname = wb.SheetNames[0];
                    const ws = wb.Sheets[wsname];
                    /* Convert array of arrays */
                    const data = utils.sheet_to_json(ws, { header: 0, blankrows: true, defval: undefined }) as Row[];
                    /* Update state */
                    console.log(data)
                    // this.setState({ data: data, cols: make_cols(ws['!ref']) });
                    setData(data)
                }
            };
            if (rABS) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);
        }

    }, [])
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })
    const [capTableFactory] = useCapTableFactory()
    const [propertyCDPFactory] = usePropertyCDPFactory()
    const [capTableQue] = useCapTableQue()
    const [capTableRegistry] = useCapTableRegistry()
    const [ERC1820] = useERC1820RegistryDeployable()
    const [entityRegistry] = useEntityRegistry()
    const [message, setMessage] = useState("");

    const possible_right_holders = [
        {
            name: "Intel Corp",
            address: "0xC293ad28C1b6CA0B28843141A30579D9A43f2212",
            id: "10001"
        },
        {
            name: "Canon KK",
            address: "0xBCDFa131B47AB1Cf2E98bb1E9B8F5F84D96E918B",
            id: "10002"
        },
        {
            name: "Blockchangers AS",
            address: "0x9c3EbA8A59C05d13883215549b8Dfe04b2Bf782a",
            id: "10003"
        },
        {
            name: "BOE Technology Group CO Ltd",
            address: "0xacAf9151521FdBe920f5c5B3406117805066c8BD",
            id: "10004"
        }
    ]

    const getRights = (str: string) => {
        const regex = /[A-Z]{2}\s[0-9]*\s\([0-9]*\)/gm;
        let m;
        let rights: string[] = [];
        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            m.forEach((match, groupIndex) => {
                rights.push(match.substr(0, 31))
            });
        }
        return rights
    }

    const getActorIdAndName = (str: string): { id: string, name: string } => {
        const regex = /([0-9]{4,8})\s(.*)/gm;
        let m;
        let idAndName: string[] = [];
        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            m.forEach((match, groupIndex) => {
                idAndName.push(match)
            });
        }
        return {
            id: idAndName[1].substr(0, 31),
            name: idAndName[2].substr(0, 31)
        }
    }

    const getAmount = (str: string) => {
        const regex = /(\d+[,|\s]\d+[,|\s]\d+)+/g;
        let m;
        let amount: string[] = [];
        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            m.forEach((match, groupIndex) => {
                amount.push(match)
            });
        }
        if (amount[0]) {
            let number = parseInt(amount[0].replace(",", "").replace(" ", ""))
            return isNaN(number) ? 1000 : number
        } else {
            return 1000
        }
    }

    const subscribed = useRef(true)
    const loading = useRef(false)
    useEffect(() => {
        const doAsync = async () => {
            if (capTableFactory && capTableQue && ERC1820 && entityRegistry && propertyCDPFactory && capTableRegistry && data.length > 0 && !loading.current) {
                loading.current = true
                console.log("##############RUNNING SOMETHING#############")
                // TODO : Integrate this as some kind of init
                let registryAddressForQue = await capTableQue.contract.getRegistry();
                console.log("registryAddressForQue ===== ", registryAddressForQue)
                if (registryAddressForQue === ethers.constants.AddressZero) {
                    setMessage("Setting registry for que")
                    let txSetRegistry = await capTableQue.contract.setRegistry(capTableRegistry.contract.address);
                    await txSetRegistry.wait()
                }
                let controllersForRegistry = await capTableRegistry.contract.controllers();
                console.log("controllersForRegistry ===== ", controllersForRegistry)
                if (controllersForRegistry.indexOf(capTableQue.contract.address) === -1) {
                    setMessage("Making que controller for Registry")
                    let txQueControllerForRegistry = await capTableRegistry.contract.setControllers([capTableQue.contract.address, await capTableQue.contract.signer.getAddress()])
                    await txQueControllerForRegistry.wait()
                }
                for (var i = 0; i < 12; i++) {
                    let rightTx = data[i]
                    // TODO : Only for testing
                    // if (rightTx.Type !== "LIs") {
                    //     break;
                    // }
                    let rights = getRights(rightTx["Associated IP Rights"])
                    const partition = ethers.utils.formatBytes32String("OWNERSHIP")

                    // Create pledgee
                    const pledgee = getActorIdAndName(rightTx["Licensee/Pledgee"])
                    console.log("rightTx", rightTx)
                    console.log("pledgee.name", pledgee.name)
                    let pledgeeAddress = await entityRegistry.addressForName(pledgee.name)
                    if (pledgeeAddress === ethers.constants.AddressZero) {
                        let pledgeeAddressPromise = entityRegistry.newAddressForId(pledgee.id)
                        console.log(rightTx["Licensee/Pledgee"], pledgee)
                        setMessage("Creating pledgee " + pledgee.name)
                        const txPledgee = await entityRegistry.suggest(pledgee.id, pledgee.name)
                        await txPledgee
                        pledgeeAddress = await pledgeeAddressPromise
                    }

                    // Create pledgor
                    const pledgor = getActorIdAndName(rightTx["Licensor/Pledgor"])
                    let pledgorAddress = await entityRegistry.addressForName(pledgor.name)
                    if (pledgorAddress === ethers.constants.AddressZero) {
                        let pledgorAddressPromise = entityRegistry.newAddressForId(pledgor.id)
                        setMessage("Creating pledgor " + pledgor.name)
                        const txPledgor = await entityRegistry.suggest(pledgor.id, pledgor.name)
                        await txPledgor
                        pledgorAddress = await pledgorAddressPromise
                    }


                    // Create rights
                    for (var y = 0; y < rights.length; y++) {
                        let right = rights[y];
                        setMessage("Creating right" + right)

                        // Create right table
                        let capTable = await capTableFactory.deploy(
                            right,
                            right.substr(3, 6),
                            1,
                            ["0x81a6Fe25534cb713DFDd75e5DD49bA5c02EDe148"],
                            "0x81a6Fe25534cb713DFDd75e5DD49bA5c02EDe148",
                            false,
                            [partition],
                            capTableQue.contract.address,
                            ERC1820.contract.address,
                            ethers.utils.formatBytes32String(right.substr(0, 31)),
                            // {
                            //     gasLimit: 8000000
                            // }
                        )
                        await capTable.deployed()

                        // const delay = (ms: number) => {
                        //     return new Promise(resolve => setTimeout(resolve, ms));
                        // }
                        // await delay(2000);


                        if (rightTx.Type === "Pledge") {
                            // Create CDP
                            setMessage("Creating CDP for " + right)
                            const amount = getAmount(rightTx.Remarks)
                            console.log("Amount => ", amount)
                            const propertyCDP = await propertyCDPFactory.deploy(ERC1820.contract.address);
                            await propertyCDP.deployed()
                            setMessage("Setting CDP entity")
                            let CDPentity = await entityRegistry.set(ethers.utils.formatBytes32String(propertyCDP.address.substr(0, 31)), "CDP for" + right, propertyCDP.address)
                            await CDPentity.wait()
                            setMessage("Issuing CDP")
                            const transferIssueByPartition = await capTable.issueByPartition(partition, propertyCDP.address, 1, "0x4341505f5441424c455f4344505f464c41470000000000000000000000000000" +
                                NumToHexBytes32(amount) +
                                addressToBytes32(pledgorAddress) +
                                addressToBytes32(pledgeeAddress))
                            await transferIssueByPartition.wait();

                            // Upload CDP data 
                            setMessage("Saving CDP data")
                            let db = firestore()
                            let data = {
                                regNr: rightTx["Reg Nr."],
                                lapseDate: rightTx["Lapse date"] ? rightTx["Lapse date"] : "",
                                limitations: rightTx.Limitations ? rightTx.Limitations : "",
                                remarks: rightTx.Remarks ? rightTx.Remarks : "",
                                regStatus: rightTx["Reg.status"] ? rightTx["Reg.status"] : "",
                                validFrom: rightTx["Valid from"] ? rightTx["Valid from"] : "",
                                rightAddress: capTable.address,
                                cdpAddress: propertyCDP.address
                            }
                            console.log("Data", data)
                            await db
                                .collection("cdp")
                                .doc(propertyCDP.address)
                                .set(data, { merge: true });

                        } else {
                            setMessage("Creating regular right")
                            let rng = (Math.floor(Math.random() * 4) + 1) - 1;
                            let owner = possible_right_holders[rng];
                            let ownerAddress = await entityRegistry.addressForName(owner.name)
                            if (ownerAddress === ethers.constants.AddressZero) {
                                setMessage("Creating entity " + owner.name)
                                const newEntity = await entityRegistry.set(owner.id, owner.name, owner.address)
                                await newEntity.wait()
                                ownerAddress = owner.address
                            }
                            setMessage("Issuing ownership to " + owner.name)
                            let issue = await capTable.issueByPartition(partition, owner.address, 1, "0x11")
                            await issue.wait()
                        }

                        // Upload data 
                        setMessage("Saving data")
                        let db = firestore()
                        let data = {

                        }
                        console.log("Data", data)
                        await db
                            .collection("right")
                            .doc(capTable.address)
                            .set(data, { merge: true });



                        // Accept into registry
                        setMessage("Accepting right into Registry")
                        let txProcess = await capTableQue.contract.process(capTable.address, true, ethers.utils.formatBytes32String("Accepted when uploading db"))
                        console.log(txProcess)
                        await txProcess.wait()

                    }






                }
                setMessage("")
            }
        };
        doAsync();
    }, [data, capTableFactory, capTableQue, ERC1820, entityRegistry, propertyCDPFactory, capTableRegistry, possible_right_holders])

    return (
        <Box>
            <Box {...getRootProps()} background="grey" style={{ minHeight: "100px" }}>
                <input {...getInputProps()} />
                {isDragActive ?
                    <p>Drop the files here ...</p> :
                    <p>Drag 'n' drop some files here, or click to select files</p>
                }
                <Box>
                    {message.length > 0 &&
                        < Box align="center" >
                            <SpinnerDiamond color="brand"></SpinnerDiamond>
                            <Text>{message}</Text>
                        </Box>
                    }
                </Box>
            </Box>

        </Box >
    )
}