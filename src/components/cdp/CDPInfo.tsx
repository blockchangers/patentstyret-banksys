import { ethers } from 'ethers';
import { Box, Grid, Text, TextArea } from 'grommet';
import React, { useEffect, useState } from 'react';
import { CDPData } from '../../symf/CapTable';
import { useCapTable } from '../../symf/Symf';
interface Props {
    rightAddress: string,
    cdpAddress: string
}

export interface CapTableData {
    address: string
    name: string
    uuid: string
    totalSupply: string
}

export const CDPInfo: React.FC<Props> = ({ rightAddress, cdpAddress }) => {
    const [capTable] = useCapTable({ address: rightAddress })
    const [capTableData, setCapTableData] = useState<CapTableData>();
    const [cdpData, setCDPData] = useState<CDPData>();

    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                const name = await capTable.name().catch(() => "No company found");
                const totalSupplyBN = await capTable.contract.totalSupply()
                    .catch(() => ethers.constants.Zero);
                const totalSupply = totalSupplyBN.toString();
                const uuidBytes32 = await capTable.contract.getUuid()
                const uuid = ethers.utils.parseBytes32String(uuidBytes32)
                setCapTableData({ address: rightAddress, name, uuid, totalSupply });
            }
        };
        doAsync();
    }, [rightAddress, capTable])


    useEffect(() => {
        let subscribed = true;
        const doAsync = async () => {
            if (capTable) {
                let data = await capTable.getCDPdata(cdpAddress)
                if (subscribed && data) {
                    setCDPData(data)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [capTable, cdpAddress])

    return (
        <Box gap="small">
            {/* {data &&
                <Grid columns={["small", "flex"]}>
                    <Text>Navn</Text>
                    <Text weight="bold">{data.name}</Text>
                </Grid>
            } */}
            {capTableData &&
                <Grid columns={["small", "flex"]}>
                    <Text >Avtaler</Text>
                    <Text weight="bold">{capTableData.totalSupply}</Text>
                </Grid>
            }
            {cdpData &&
                <Box gap="small">
                    <Grid columns={["small", "flex"]}>
                        <Text >RegNr.</Text>
                        <Text weight="bold">{cdpData.regNr}</Text>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Reg status</Text>
                        <Text weight="bold">{cdpData.regStatus}</Text>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Valid from</Text>
                        <Text weight="bold">{cdpData.validFrom}</Text>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Lapse data</Text>
                        <Text weight="bold">{cdpData.lapseDate}</Text>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Limitations</Text>
                        <TextArea value={cdpData.limitations}></TextArea>
                    </Grid>
                    <Grid columns={["small", "flex"]}>
                        <Text >Remarks</Text>
                        <TextArea value={cdpData.remarks}></TextArea>
                    </Grid>
                </Box>
            }

        </Box>
    )
}