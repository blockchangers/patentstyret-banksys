/* eslint-disable react-hooks/exhaustive-deps */
import { Box, Button, DataTable, Grid, Heading, Text, TextInput } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { SpinnerDiamond } from 'spinners-react';
import { firestore } from '../../symf/dependencies/Firestore';
import { Visibility } from '../ui/Visibility';


interface Props { }

export const CDPAgreementList: React.FC<Props> = () => {
    const history = useHistory();
    const [/* pageOffset */, /* setListOffset */] = useState(0);
    const [pageLimit, setpageSize] = useState(10);
    const [loading, setLoading] = useState(false);
    const [agreements, setAgreements] = useState<any[]>([]);


    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            setLoading(true)
            const db = firestore()
            let list = await db.collection("agreement").get()
            list.forEach(doc => {
                let data = doc.data() as any
                if (subscribed && data.pledgor && data.pledgee) {
                    setAgreements(old => [...old, data])
                }
            })
            setLoading(false)
        };
        doAsync();
        return () => { subscribed = false }
    }, [])
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'NOK',

        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });

    return (
        <Box align="center" gap="small">
            <Heading>Received agreements</Heading>
            <Box direction="column" gap="small">
                <Visibility visibilty="context" />
                <Grid gap="small" columns={{ count: 2, size: "auto" }}>
                    <Grid columns={["xsmall", "flex"]} alignContent="center">
                        <Text alignSelf="end">Nr.</Text>
                        <TextInput size="small" type="number" value={pageLimit} onChange={e => setpageSize(parseInt(e.target.value))}></TextInput>
                    </Grid>

                    <Button size="small" label="Propose new agreement" onClick={() => { history.push("/agreement/cdp/") }}></Button>

                </Grid>
            </Box>
            {agreements.length > 0
                ?
                <DataTable
                    onClickRow={(event) => {
                        history.push("/agreement/cdp/" + event.datum.id);
                    }}
                    columns={[
                        {
                            property: "id.",
                            header: <Text>#</Text>,
                            render: (data) => (<Text>{data.id.substr(0, 5)}</Text>)
                        },
                        {
                            property: "Avtale",
                            header: <Text>Description</Text>,
                            render: (data) => (<Text>{"Pledge agreement / " + data.pledgee + " & " + data.pledgor}</Text>)
                        },
                        {
                            property: "Verdi",
                            header: <Text>Total value</Text>,
                            render: (data) => (<Text>{formatter.format(data.totalValue)}</Text>)
                        },
                        {
                            property: "status",
                            header: <Text>Status</Text>,
                            render: (data) => (<Text>{data.status}</Text>)
                        },
                    ]}
                    data={agreements}
                />
                : (loading ?
                    <Box align="center">
                        <SpinnerDiamond color="brand"></SpinnerDiamond>
                        <Text>Henter rettigheter</Text>
                    </Box>
                    :
                    <Text>Tom liste</Text>
                )
            }

        </Box>
    )
}