import React, { useEffect } from 'react';
import { Box, Grid, Text, TextInput, Button } from 'grommet';
import { Controller, useForm } from 'react-hook-form';
import { useEntityRegistry } from '../../symf/Symf';

interface Props {
    onComplete: (data: FormData) => void
}

interface FormData {
    address: string,
    id: string,
    name: string
}

export const CreateEntity: React.FC<Props> = ({ onComplete }) => {

    const [entityRegistry] = useEntityRegistry()
    const { handleSubmit, control, setValue, formState } = useForm<FormData>({
        defaultValues: {
            address: "",
            id: "",
            name: ""
        }
    })

    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (entityRegistry) {
                let address = await entityRegistry.contract.signer.getAddress()
                if (subscribed) {
                    setValue("address", address)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [entityRegistry, setValue])

    useEffect(() => {
        setValue("id", Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5))
    }, [setValue])


    const onSubmit = async (data: FormData) => {
        console.log("submit data", data);
        if (entityRegistry) {
            let tx = await entityRegistry.set(data.id, data.name, data.address)
            await tx.wait()
            onComplete(data)
        }
    }
    return (
        <Box gap="small">
            <Text>You dont seem to have an entity with NIPO. Please create this first.</Text>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Box gap="small">
                    <Grid columns={["small", "auto"]} gap="small" align="center">
                        <Text>Address</Text>
                        <Controller as={<TextInput disabled={true} ></TextInput>} name="address" control={control} rules={{ required: false }} />
                    </Grid>

                    <Grid columns={["small", "auto"]} gap="small" align="center">
                        <Text>ID</Text>
                        <Controller as={<TextInput disabled={true} ></TextInput>} name="id" control={control} rules={{ required: false }} />
                    </Grid>

                    <Grid columns={["small", "auto"]} gap="small" align="center">
                        <Text>Name</Text>
                        <Controller as={<TextInput disabled={false} ></TextInput>} name="name" control={control} rules={{ required: false }} />
                    </Grid>

                    <Grid /* columns={["small", "auto"]} */ gap="small" align="center">
                        <Button type="submit" primary label="Opprett min entitet" disabled={formState.isSubmitting || !formState.isDirty} />
                    </Grid>
                </Box>
            </form>
        </Box>
    )
}