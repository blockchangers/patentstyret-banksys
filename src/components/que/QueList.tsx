/* eslint-disable react-hooks/exhaustive-deps */
import { ethers, BigNumber } from 'ethers';
import { Box, DataTable, Heading, RadioButtonGroup, Text, TextInput, Grid } from 'grommet';
import React, { useEffect, useState, useRef } from 'react';
import { useHistory } from "react-router-dom";
import { SpinnerDiamond } from 'spinners-react';
import { useCapTableQue, useCapTableFactory } from '../../symf/Symf';
import { formatBN } from '../../utils/formatNumbers';


interface Props { }

export const QueList: React.FC<Props> = () => {
    const [capTableQue] = useCapTableQue()
    const [capTableFactory] = useCapTableFactory()
    const [data, setData] = useState<{ address: string, name: string, totalSupply: BigNumber }[]>([]);
    const history = useHistory();
    const [pageOffset, /* setListOffset */] = useState(0);
    const [pageLimit, setpageSize] = useState(10);
    const [listType, setListType] = useState("qued");
    const [loading, setLoading] = useState(false);
    const _isMounted = useRef(true);

    useEffect(() => {
        return () => {
            _isMounted.current = false;
        }
    }, []);

    useEffect(() => {
        if (capTableQue && capTableFactory) {
            updateList("qued")
        }
    }, [capTableQue, capTableFactory]);

    // Trigger update when pageOffset or pageLimit changes
    useEffect(() => {
        updateList(listType)
    }, [pageOffset, pageLimit]);

    const updateList = async (type: "qued" | "approved" | "declined" | string) => {
        console.log("updateList to type: ", type, capTableQue);
        setListType(type)
        setLoading(true)
        setData([])
        if (capTableQue) {
            if (type === "qued")
                setList(Array.from(await capTableQue.contract.listQued().catch(() => [])))
            else if (type === "approved")
                setList(Array.from(await capTableQue.contract.listApproved().catch(() => [])))
            else if (type === "declined")
                setList(Array.from(await capTableQue.contract.listDeclined().catch(() => [])))
            else
                setList([])
        }
    }

    const setList = async (list: string[]) => {
        if (_isMounted.current && capTableFactory) {
            let addresses = await list.reverse().slice(pageOffset, Math.min(list.length, pageOffset + pageLimit)).filter((a, i) => list.indexOf(a) === i)
            addresses.forEach(address => capTableData(address))
            // Render delay so empty list isent shown for 1 sec
            setTimeout(() => {
                setLoading(false)
            }, 1000)
        }
    }

    const capTableData = async (address: string) => {
        if (_isMounted.current && capTableFactory) {
            const capTable = capTableFactory.attach(address)
            const name = await capTable.name().catch(() => "No company found");
            // const symbol = await capTable.symbol().catch(() => "No symbol found");
            const totalSupplyBN = await capTable
                .totalSupply()
                .catch(() => ethers.constants.Zero);
            setData(old => [...old, { address, name, totalSupply: totalSupplyBN }])
        }
    };


    return (
        <Box align="center" gap="small">
            <Heading>Kø for rettigheter</Heading>
            <Box direction="column" gap="small">
                <RadioButtonGroup
                    direction="row"
                    name="doc"
                    options={[{
                        value: 'qued',
                        label: 'Kø'
                    }, {
                        value: 'approved',
                        label: 'Godkjent'
                    }, {
                        value: 'declined',
                        label: 'Avslått'
                    }]}
                    value={listType}
                    onChange={(event: any) => updateList(event.target.value)}
                />
                <Grid columns={["xsmall", "auto"]}>
                    <Text alignSelf="end">Antall</Text>
                    <TextInput size="small" type="number" value={pageLimit} onChange={e => setpageSize(parseInt(e.target.value))}></TextInput>
                </Grid>
            </Box>
            {data.length > 0
                ?
                <DataTable
                    onClickRow={(event: any) => {
                        history.push("/right/" + event.datum.address);
                    }}
                    columns={[
                        {
                            property: "address",
                            header: <Text>#</Text>,
                            render: (data) => (<Text>{data.address.substr(0, 4) + ".."}</Text>)
                        },
                        {
                            property: 'name',
                            header: <Text>Navn</Text>,
                            "search": true,
                        },
                        {
                            property: 'totalSupply',
                            header: <Text>Avtaler</Text>,
                            align: "center",
                            render: (data) => formatBN(data.totalSupply)

                        },

                    ]}
                    data={data}
                />
                : (loading ?
                    <Box align="center">
                        <SpinnerDiamond color="brand"></SpinnerDiamond>
                        <Text>Henter rettigheter</Text>
                    </Box>
                    :
                    <Text>Tom liste</Text>
                )
            }

        </Box>
    )
}