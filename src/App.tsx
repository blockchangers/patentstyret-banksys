import { Box, Grid, Grommet, Heading } from 'grommet';
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Navigation } from './components/Navigation';
import { QueList } from './components/que/QueList';
import { RegistryList } from './components/registry/RegistryList';
import { UploadDB } from './components/UploadDB';
import { CreateAgreementCDPPage } from './pages/CreateAgreementCDPPage';
import { Home } from './pages/Home';
import { RightPage } from './pages/RightPage';
import { Theme } from './utils/theme';
import { CDPPage } from './pages/CDPPage';
import { CDPAgreementList } from './components/cdp/CDPAgreementList';
import { AnnouncmentPage } from './pages/AnnouncmentPage';

export const DarkTheme = React.createContext<[boolean, (boolean: boolean) => void]>([false, (arg: boolean) => { }])

declare global {
  interface Window { ethereum: any }
}
if (window.ethereum) {
  window.ethereum["autoRefreshOnNetworkChange"] = false
}


function App() {
  const [darkTheme, setDarkTheme] = useState(false);

  return (
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <DarkTheme.Provider value={[darkTheme, setDarkTheme]} />
      <Grommet theme={Theme} themeMode={darkTheme ? "dark" : "light"} full>
        <Box background="white" fill="vertical" >
          <Grid
            rows={["auto", "xlarge", "small"]}
            columns={["full"]}
            areas={[
              { name: "header", start: [0, 0], end: [1, 0] },
              { name: "main", start: [0, 1], end: [1, 1] },
              { name: "footer", start: [0, 2], end: [1, 2] }
            ]}
          // fill="vertical"
          >
            {/* HEADER */}
            <Box pad={{ horizontal: "small" }} background="background-front" gridArea="header" align="center" direction="row" gap="medium" fill="vertical" flex={false} justify="between" >

              <Navigation></Navigation>
            </Box>
            {/* MAIN */}
            <Box pad="small" gridArea="main" fill="vertical" flex="grow" overflow="auto" height={{ min: "large" }}>
              <Switch>
                <Route exact path="/">
                  <Home />
                </Route>
                <Route path="/agreement/cdp/:id?">
                  <CreateAgreementCDPPage />
                </Route>
                <Route exact path="/admin/db/upload">
                  <UploadDB />
                </Route>
                <Route exact path="/que">
                  <QueList />
                </Route>
                <Route exact path="/registry">
                  <RegistryList />
                </Route>
                <Route exact path="/right/:address">
                  <RightPage></RightPage>
                </Route>
                <Route path="/right/:rightAddress/:cdpAddress">
                  <CDPPage></CDPPage>
                </Route>
                <Route path="/agreement/list">
                  <CDPAgreementList></CDPAgreementList>
                </Route>
                <Route path="/announcement/list">
                  <AnnouncmentPage></AnnouncmentPage>
                </Route>
                <Route path="*">
                  <p>404 No page found</p>
                </Route>
              </Switch>
            </Box>
            {/* FOOTER */}
            <Box gridArea="footer" align="center" direction="row" gap="medium" fill="vertical" /* flex={false} */ justify="between" >
              <Box margin={{ top: "large" }} background="brand" align="center" justify="center" alignContent="center" fill="horizontal" style={{ minHeight: "200px" }} gap="large">
                <Heading level="3">Provided by Norwegian Industrial Property Office</Heading>
              </Box>
            </Box>

          </Grid>
        </Box>
      </Grommet>
    </BrowserRouter>
  );
}

export default App;
