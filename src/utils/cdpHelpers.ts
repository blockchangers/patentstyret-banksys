import { BigNumber, ethers } from "ethers";
import { useEffect, useState } from "react";
import { Erc20MintableBurnable } from "../symf/dependencies/typechain/Erc20MintableBurnable";
import {
  useCapTable,
  useERC20MintableBurnable,
  useERC20MintableBurnableFactory,
  usePropertyCDPFactory,
} from "./../symf/Symf";
import { CapTable } from "../symf/dependencies/typechain/CapTable";

export interface CollateralDetail {
  cToken: {
    balance: BigNumber;
    holder: string;
  }[];
  pToken: {
    balance: BigNumber;
    holder: string;
  }[];
  closed: boolean;
  cdpAddress: string;
}
export interface CollateralDetails {
  [tokenHolder: string]: CollateralDetail;
}

export const useERC20Addresses = (address: string) => {
  const [token] = useERC20MintableBurnable({ address });
  const [addresses, setAddresses] = useState<string[]>();
  const [loading, setLoading] = useState(true);
  const [error /* _setError */] = useState("");
  useEffect(() => {
    let subscribed = true;
    const doAsync = async () => {
      if (subscribed && token) {
        let addresses = await getERC20Addresses(token.contract, address);
        setAddresses(addresses);
        setLoading(false);
      }
    };
    doAsync();
    return () => {
      subscribed = false;
    };
  }, [address, token]);

  return [addresses, loading, error];
};

export const getERC20Addresses = async (
  token: Erc20MintableBurnable,
  address: string
) => {
  const logs = await token.signer.provider
    ?.getLogs({
      fromBlock: 0,
      address: address,
      toBlock: "latest",
      topics: [ethers.utils.id(`Transfer(address,address,uint256)`)],
    })
    .finally(() => []);

  if (logs) {
    console.log("logs => ", logs);
    const events = logs.map((log) => {
      return { ...token.interface.parseLog(log) };
    });
    console.log("events => ", events);

    const addresses = events
      .map((event) => {
        return event.args["to"];
      })
      .reduce(
        (prev, cur) => (prev.indexOf(cur) === -1 ? [...prev, cur] : prev),
        []
      ) as string[];

    return addresses;
  }
};

export const getERC1400Addresses = async (
  capTable: CapTable,
  address: string,
  partitionFilter = ""
) => {
  const logs1 = await capTable.signer.provider
    ?.getLogs({
      fromBlock: 0,
      address: address,
      toBlock: "latest",
      topics: [
        ethers.utils.id(
          `TransferByPartition(bytes32,address,address,address,uint256,bytes,bytes)`
        ),
      ],
    })
    .finally(() => []);

  const logs2 = await capTable.signer.provider
    ?.getLogs({
      fromBlock: 0,
      address: address,
      toBlock: "latest",
      topics: [
        ethers.utils.id(
          "IssuedByPartition(bytes32,address,address,uint256,bytes,bytes)"
        ),
      ],
    })
    .finally(() => []);

  const logs3 = await capTable.signer.provider
    ?.getLogs({
      fromBlock: 0,
      address: address,
      toBlock: "latest",
      topics: [
        ethers.utils.id(
          "RedeemedByPartition(bytes32,address,address,uint256,bytes,bytes)"
        ),
      ],
    })
    .finally(() => []);

  if (logs1 && logs2 && logs3) {
    const logs = [...logs1, ...logs2, ...logs3];

    // console.log("logs => ", logs);

    const events = logs.map((log) => {
      return { ...capTable.interface.parseLog(log) };
    });
    // console.log("events => ", events);

    // Only need .to addresses to get every possible token holder
    // filter out duplicate values
    const partitionFilterBytes32 =
      partitionFilter && partitionFilter.substr(0, 2) !== "0x"
        ? ethers.utils.formatBytes32String(partitionFilter)
        : partitionFilter;
    const addresses = events
      .filter((event) => {
        if (partitionFilter) {
          if (
            event.args["partition"] === partitionFilterBytes32 ||
            event.args["fromPartition"] === partitionFilterBytes32 ||
            event.args["toPartition"] === partitionFilterBytes32
          ) {
            return true;
          } else {
            return false;
          }
        }
        return true;
      })
      .map((event) => {
        return event.args["to"];
      })
      .reduce(
        (prev, cur) => (prev.indexOf(cur) === -1 ? [...prev, cur] : prev),
        []
      );
    return addresses as string[];
  }
  return [];
};
export const useERC1400Addresses = (
  address: string,
  partitionFilter = ""
): [string[], boolean, string] => {
  const [addresses, setAddresses] = useState<string[]>([]);
  const [loading, setLoading] = useState(true);
  const [error /* setError */] = useState("");
  const [capTable] = useCapTable({ address });

  useEffect(() => {
    let subscribed = true;
    const doAsync = async () => {
      if (subscribed && capTable && capTable.contract.signer.provider) {
        const addresses = await getERC1400Addresses(
          capTable.contract,
          address,
          partitionFilter
        );
        if (subscribed) {
          setAddresses(addresses);
          setLoading(false);
        }
      }
    };
    doAsync();
    return () => {
      subscribed = false;
    };
  }, [address, capTable, partitionFilter]);

  return [addresses, loading, error];
};

export const useCDPtools = () => {
  const [propertyCDPFactory] = usePropertyCDPFactory();
  const [erc20MintableBurnableFactory] = useERC20MintableBurnableFactory();

  const filterCDPholders = async (addresses: string[]): Promise<string[]> => {
    if (propertyCDPFactory) {
      const ERC1400_TOKENS_RECIPIENT = ethers.utils.solidityKeccak256(
        ["string"],
        ["ERC1400TokensRecipient"]
      );
      const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(
        ["string"],
        ["ERC1820_ACCEPT_MAGIC"]
      );
      const holderPromises = addresses.map(async (address) => {
        try {
          const maybePropertyCDP = await propertyCDPFactory.attach(address);
          const bytesAcceptMagicMaybe = await maybePropertyCDP.canImplementInterfaceForAddress(
            ERC1400_TOKENS_RECIPIENT,
            address
          );
          if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC) return address;
          else return "";
        } catch (error) {
          return "";
        }
      });
      const holdersWithEmpty = await Promise.all(holderPromises);
      const holders = holdersWithEmpty.filter((maybeString) => maybeString);
      return holders;
    } else {
      throw Error(
        "propertyCDPFactory was not initialized when filterCDPholder"
      );
    }
  };

  const getColleralDetails = async (
    addresses: string[]
  ): Promise<CollateralDetails> => {
    // let collateralDetailsObjectArray = [];
    // for (const address of addresses) {
    if (propertyCDPFactory && erc20MintableBurnableFactory) {
      const promises = addresses.map(async (address) => {
        console.log("Getting collateral details for ", address);

        const propertyCDP = await propertyCDPFactory.attach(address);
        const cTokenAddress = await propertyCDP.getCToken();
        const pTokenAddress = await propertyCDP.getPToken();

        const cToken = await erc20MintableBurnableFactory.attach(cTokenAddress);
        const pToken = await erc20MintableBurnableFactory.attach(pTokenAddress);

        const cTokenAddresses = await getERC20Addresses(cToken, cTokenAddress);
        const pTokenAddresses = await getERC20Addresses(pToken, pTokenAddress);
        if (!cTokenAddresses || !pTokenAddresses) {
          throw Error("Could not get cTokenAddresses or pTokenAddresses");
        }

        const cTokenBalances = cTokenAddresses.map(async (address) => {
          return {
            holder: address,
            balance: await cToken.balanceOf(address),
          };
        });
        const cTokenDetails = await Promise.all(cTokenBalances);
        const cTokenDetailsFiltered = cTokenDetails.filter((tokenDetail) => {
          if (tokenDetail.balance.isZero()) {
            return false;
          }
          return true;
        });
        const pTokenBalances = pTokenAddresses.map(async (address) => {
          return {
            holder: address,
            balance: await pToken.balanceOf(address),
          };
        });
        const pTokenDetails = await Promise.all(pTokenBalances);
        const pTokenDetailsFiltered = pTokenDetails.filter((tokenDetail) => {
          if (tokenDetail.balance.isZero()) {
            return false;
          }
          return true;
        });
        const collateralDetail: CollateralDetail = {
          closed: await propertyCDP.closed(),
          cToken: cTokenDetailsFiltered,
          pToken: pTokenDetailsFiltered,
          cdpAddress: address,
        };
        // collateralDetailsObjectArray.push({ [address]: collateralDetail });
        return { [address]: collateralDetail };
      });
      const collateralDetailsObjectArray = await Promise.all(promises);
      const collateralDetails = collateralDetailsObjectArray.reduce(
        (prev, curr) => {
          return { ...prev, ...curr };
        },
        {}
      );
      return collateralDetails;
    } else {
      throw Error(
        "propertyCDPFactory or erc20MintableBurnableFactory was not initialized when getColleralDetails"
      );
    }
  };

  return {
    filterCDPholders,
    getColleralDetails,
  };
};
