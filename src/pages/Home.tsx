import React from 'react';
import { Heading, Text, Box } from 'grommet';

interface Props { }

export const Home: React.FC<Props> = () => {

    return (
        <Box align="center">
            <Heading level={3}>Welcome to <Text size="xxlarge" weight="bold" style={{ fontStyle: "italic" }}>Rights management</Text></Heading>
        </Box>
    )
}