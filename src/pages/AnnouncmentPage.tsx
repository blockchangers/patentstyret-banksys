/* eslint-disable react-hooks/exhaustive-deps */
import { ethers } from 'ethers';
import { Box, Button, DataTable, Grid, Heading, Text, TextInput } from 'grommet';
import { More } from 'grommet-icons';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { SpinnerDiamond } from 'spinners-react';
import { Visibility } from '../components/ui/Visibility';
import { useCapTableFactory, useCapTableRegistry, useEntityRegistry, usePropertyCDPFactory } from '../symf/Symf';

interface Props { }

interface EventRow {
    url: string,
    address: string
    message: string,
    blockNumber: number,
    txHash: string,
}

enum STATE {
    DEFAULT,
    FETCHING,
    DONE
}

export const AnnouncmentPage: React.FC<Props> = () => {
    const history = useHistory()

    const [capTableRegistry] = useCapTableRegistry()
    const [capTableFactory] = useCapTableFactory()
    const [propertyCDPFactory] = usePropertyCDPFactory()
    const [entityRegistry] = useEntityRegistry()

    const [data, setData] = useState<EventRow[]>([]);
    const [loadingMessage, setLoadingMessage] = useState("Initiating contracts");
    const [state, setState] = useState(STATE.DEFAULT);
    const [blockBack, setBlockBack] = useState(100);


    const getRightAddedEvent = async () => {
        setLoadingMessage("Fetching list of rights")
        if (capTableRegistry && capTableFactory) {
            const currentBlock = await capTableRegistry.contract.provider.getBlockNumber()
            const blockLookback = currentBlock - blockBack
            const ethEvents = await capTableRegistry.contract.queryFilter(capTableRegistry.contract.filters.capTableAdded(null), blockLookback, "latest")
            // let events = Promise.all(promises)
            let events = ethEvents.reverse().map(e => {
                let address = e.args ? e.args[0] : "0x"
                return {
                    url: "/right/" + address,
                    address: address,
                    blockNumber: e.blockNumber,
                    txHash: e.transactionHash,
                    message: "Right created"
                }
            })
            console.log(events)
            setLoadingMessage("Fetcing right names")
            let subEvents: EventRow[] = []
            for (let i = 0; i < events.length; i++) {
                try {
                    // get name
                    const capTable = await capTableFactory.attach(events[i].address)
                    const name = await capTable.name()
                    events[i].message = "Right " + name + " created"

                    // Get txs
                    const issueEvents = await capTable.queryFilter(capTable.filters.IssuedByPartition(null, null, null, null, null, null), blockLookback, "latest")
                    // const redeemEvents = await capTable.queryFilter(capTable.filters.RedeemedByPartition(null, null, null, null, null, null), blockLookback, "latest")
                    const transferEvents = await capTable.queryFilter(capTable.filters.TransferByPartition(null, null, null, null, null, null, null), blockLookback, "latest")
                    setLoadingMessage("Fetching txs for " + name)
                    const newEventPromises = [...issueEvents, ...transferEvents].reverse().map(async e => {
                        let subAddress = e.args ? e.args[2] : "0x"
                        let partition = e.args ? ethers.utils.parseBytes32String(e.args[0]) : ""
                        let cdpHolder = await isCDPHolder(subAddress)
                        let message = "Right has been changed"
                        if (cdpHolder) {
                            message = "Pledge has been issued."
                        } else if (partition.toLowerCase().includes("lisence")) {
                            message = "Lisens har blitt utstedt på rettighet"
                        } else {
                            message = "Agreement for right changed"
                        }
                        return {
                            url: "/right/" + events[i].address,
                            address: subAddress,
                            blockNumber: e.blockNumber,
                            txHash: e.transactionHash,
                            message: message
                        }
                    })
                    const newEvents = await Promise.all(newEventPromises)
                    subEvents = [...subEvents, ...newEvents]
                } catch (error) {
                    //
                }
            }
            return [...events, ...subEvents].sort((a, b) => {
                if (a.blockNumber > b.blockNumber) {
                    return -1
                }
                if (a.blockNumber < b.blockNumber) {
                    return 1
                }
                return 0
            })
        }
        return []

    }

    const isCDPHolder = async (address: string) => {
        const ERC1400_TOKENS_RECIPIENT = ethers.utils.solidityKeccak256(
            ["string"],
            ["ERC1400TokensRecipient"]
        );
        const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(
            ["string"],
            ["ERC1820_ACCEPT_MAGIC"]
        );

        try {
            const maybePropertyCDP = await propertyCDPFactory?.attach(address);
            const bytesAcceptMagicMaybe = await maybePropertyCDP?.canImplementInterfaceForAddress(
                ERC1400_TOKENS_RECIPIENT,
                address
            );
            if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC) return true;
            else return false;
        } catch (error) {
            return false;
        }

    }

    const updateData = async () => {
        let events = await getRightAddedEvent()
        setData(events)
    }

    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (state === STATE.DEFAULT && capTableRegistry && capTableFactory && propertyCDPFactory && entityRegistry) {
                setState(STATE.FETCHING)
                await updateData()
                if (subscribed)
                    setState(STATE.DONE)
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [state, capTableRegistry, capTableFactory, propertyCDPFactory, entityRegistry])



    return (
        <Box align="center" gap="small" margin="medium">
            <Heading>Recent announcements</Heading>
            <Visibility visibilty="public" />
            <Grid columns={["small", "small"]} margin={{ horizontal: "large" }} gap={"small"}>
                <Grid columns={["flex", "flex"]}>
                    <Text size="small" alignSelf="center">Blocks back</Text>
                    <TextInput size="small" value={blockBack} onChange={(e) => setBlockBack(parseInt(e.target.value))}></TextInput>
                </Grid>
                <Button size="small" label="Update" onClick={() => updateData()}></Button>
            </Grid>

            {data.length > 0 &&
                <DataTable
                    alignSelf="center"
                    data={data}
                    onClickRow={(event: any) => {
                        history.push(event.datum.url);
                    }}
                    primaryKey={"txHash"}
                    columns={[
                        // {
                        //     property: 'txHash',
                        //     header: "#",
                        //     primary: true,
                        //     render: (data) => data.txHash.substr(3, 2) + "..",

                        // },
                        {
                            property: 'blockNumber',
                            header: "Block Nr.",
                            sortable: true,

                        },
                        {
                            property: 'message',
                            header: "Type",

                        },
                        {
                            property: "url",
                            header: "",
                            render: (data) => <Button icon={<More></More>} onClick={() => history.push(data.url)}></Button>
                        }
                        // {
                        //     property: 'address',
                        //     header: <Text>Panthaver</Text>,
                        //     render: (data: CollateralBalance) => (
                        //         <Box gap="small" pad="small" >
                        //             {<Address2Name address={data.address}></Address2Name>}
                        //         </Box>

                        //     )
                        // },
                    ]}
                ></DataTable>
            }
            {state === STATE.FETCHING &&
                <Box align="center" margin={{ top: "medium" }} >
                    <SpinnerDiamond color="brand"></SpinnerDiamond>
                    <Heading level={4}>{loadingMessage}</Heading>
                </Box>
            }
        </Box >
    )
}