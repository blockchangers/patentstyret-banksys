import { ethers } from 'ethers';
import { Box, Button, Layer } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { CreateEntity } from '../components/entity/CreateEntity';
import { CreatePledge } from '../components/right/CreatePledge';
import { useEntityRegistry } from '../symf/Symf';

interface Props { }

export interface Holder {
    name: string,
    address: string
}


export const CreateAgreementCDPPage: React.FC<Props> = () => {
    const { id } = useParams<{ id?: string }>();
    const [agreementId, setAgreementID] = useState<string>();
    // const [capTable] = useCapTable({ address })
    // const [erc1400Addresses] = useERC1400Addresses(address)
    const [entityRegistry] = useEntityRegistry()
    const [currentAccount, setCurrentAccount] = useState<Holder>();
    const [needEntity, setNeedEntity] = useState(false);
    const [entities, setEntities] = useState<string[]>();

    // Generate agreement if if not set
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (!id) {
                const create_UUID = () => {
                    var dt = new Date().getTime();
                    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                        var r = (dt + Math.random() * 16) % 16 | 0;
                        dt = Math.floor(dt / 16);
                        return (c === 'x' ? r : (r && 0x3 | 0x8)).toString(16);
                    });
                    return uuid;
                }
                if (subscribed)
                    setAgreementID(create_UUID())
            } else {
                if (subscribed)
                    setAgreementID(id)
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [id])

    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (entityRegistry) {
                let listBytes = await entityRegistry.contract.get_name_list()
                let list = listBytes.map(b => ethers.utils.parseBytes32String(b))
                if (subscribed) {
                    setEntities(list)
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [entityRegistry])


    // Get currentHolder
    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (entityRegistry) {
                let currentAddress = await entityRegistry.contract.signer.getAddress()
                console.log(currentAddress)
                let currentName = await entityRegistry.nameForAddress(currentAddress)
                console.log(currentName)
                if (currentName === "") {
                    setNeedEntity(true)
                } else {
                    if (subscribed) {
                        setCurrentAccount({
                            name: currentName,
                            address: currentAddress
                        })
                    }
                }
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [entityRegistry])

    return (
        <Box align="center">
            {entities && agreementId && currentAccount &&
                <CreatePledge entities={entities} agreementId={agreementId}></CreatePledge>
            }
            <Box>
                {needEntity &&
                    <Layer
                        onEsc={() => setNeedEntity(false)}
                        onClickOutside={() => setNeedEntity(false)}
                    >
                        <Box pad="small" gap="small">
                            <CreateEntity onComplete={function (data) { setNeedEntity(false); setCurrentAccount({ address: data.address, name: data.name }) }}></CreateEntity>
                            <Button label="close" onClick={() => setNeedEntity(false)} />
                        </Box>
                    </Layer>
                }
            </Box>
        </Box>
    )
}