import { firestore } from 'firebase';
import { Box, Grid, Heading, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { DownloadDocumet } from '../components/ui/DownloadDocumet';
import { UIBox } from '../components/ui/UIBox';
import { Visibility } from '../components/ui/Visibility';

interface AgreementData {
    completed: { cdp: string, right: string }[]
    document: string
    id: string
    limitations: string
    pledgee: string
    pledgor: string
    remarks: string
    rights: { address: string, name: string }[]
    status: string
    totalValue: string,
    validFrom: string
}

interface Props { }


export const CDPPage: React.FC<Props> = () => {
    const { rightAddress, cdpAddress } = useParams<{ rightAddress: string, cdpAddress: string }>();
    // const [erc20Addresses] = useERC20Addresses(address)
    // const [erc1400Addresses] = useERC1400Addresses(rightAddress)
    // const [capTable] = useCapTable({ address: rightAddress });
    const [agreement, setAgreement] = useState<AgreementData>();

    // const [cdpData, setCdpData] = useState<CDPData>();
    // useEffect(() => {
    //     let subscribed = true;
    //     const doAsync = async () => {
    //         if (capTable) {
    //             let data = await capTable.getCDPdata(cdpAddress)
    //             if (subscribed && data) {
    //                 setCdpData(data)
    //             }
    //         }
    //     };
    //     doAsync();
    //     return () => { subscribed = false }
    // }, [capTable, cdpAddress])


    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            let db = firestore()
            let docRef = await db.collection("agreement").get()
            docRef.forEach(doc => {
                const data = doc.data();
                if (data.completed) {
                    data.completed.forEach(({ cdp, right }: { cdp: string, right: string }) => {
                        if (right === rightAddress && cdp === cdpAddress) {
                            console.log("Found CDP data", data)
                            if (subscribed)
                                setAgreement(data as AgreementData)
                        }
                    });
                }
            })

        };
        doAsync();
        return () => { subscribed = false }
    }, [cdpAddress, rightAddress])

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'NOK',

        // These options are needed to round to whole numbers if that's what you want.
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });


    return (
        <Box align="center">
            <Heading level="2">Pledge agreement (CDP)</Heading>
            <Visibility visibilty="context" />

            {cdpAddress &&
                <Box gap="large" >
                    <UIBox>
                        {agreement &&
                            <Box gap="small">
                                {/* <Grid columns={["small", "flex"]}>
                                    <Text >Agreement status</Text>
                                    <Text weight="bold">{agreement.status}</Text>
                                </Grid> */}
                                <Grid columns={["small", "flex"]}>
                                    <Text >Valid from</Text>
                                    <Text weight="bold">{agreement.validFrom}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Limitations</Text>
                                    <Text >{agreement.limitations}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Remarks</Text>
                                    <Text >{agreement.remarks}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Pledgee</Text>
                                    <Text >{agreement.pledgee}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Pledgor</Text>
                                    <Text >{agreement.pledgor}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Total value</Text>
                                    <Text >{formatter.format(parseInt(agreement.totalValue))}</Text>
                                </Grid>
                                <Grid columns={["small", "flex"]}>
                                    <Text >Document</Text>
                                    {agreement.document &&
                                        <DownloadDocumet docId={agreement.document}></DownloadDocumet>}
                                </Grid>


                            </Box>
                        }
                    </UIBox>
                </Box>
            }
        </Box>
    )
}