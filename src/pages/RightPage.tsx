import { Box, Grid } from 'grommet';
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { CollateralBalances } from '../components/right/CollateralBalances';
import { RightHeading } from '../components/right/RightHeading';
import { RightInfo } from '../components/right/RightInfo';
import { UIBox } from '../components/ui/UIBox';
import { useERC1400Addresses } from '../utils/cdpHelpers';

interface Props { }

export const RightPage: React.FC<Props> = () => {
    const { address } = useParams<{ address: string }>();
    // const [erc20Addresses] = useERC20Addresses(address)
    const [erc1400Addresses] = useERC1400Addresses(address)

    useEffect(() => {
        let subscribed = true
        const doAsync = async () => {
            if (subscribed) {
                console.log(erc1400Addresses)
            }
        };
        doAsync();
        return () => { subscribed = false }
    }, [erc1400Addresses])


    return (
        <Box align="center">
            {/* <Heading></Heading> */}
            <RightHeading address={address}></RightHeading>
            {address &&
                <Box gap="large" >
                    <UIBox>
                        <Grid columns={["1/2", "1/2"]} fill="horizontal">
                            <RightInfo address={address}></RightInfo>
                        </Grid>
                    </UIBox>


                    {erc1400Addresses && erc1400Addresses &&
                        <CollateralBalances address={address} tokenHolderList={erc1400Addresses}></CollateralBalances>
                    }

                    {/* <UIBox heading="Handlinger">
                        <Grid columns={["small"]}>

                            <Button label="Opprett pant" onClick={() => history.push("/right/pledge/create/" + address)}></Button>
                        </Grid>
                    </UIBox> */}

                    <UIBox heading="Lisence">
                        Coming soon
                    </UIBox>
                </Box>
            }
        </Box>
    )
}