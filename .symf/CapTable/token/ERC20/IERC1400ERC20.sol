pragma solidity >=0.5.5;


interface ERC1400ERC20 {
    function decimals() external pure returns (uint8);

    function allowance(address owner, address spender)
        external
        view
        returns (uint256);

    function approve(address spender, uint256 value) external returns (bool);

    function transfer(address to, uint256 value) external returns (bool);

    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool);

    function migrate(address newContractAddress, bool definitive) external;
}
