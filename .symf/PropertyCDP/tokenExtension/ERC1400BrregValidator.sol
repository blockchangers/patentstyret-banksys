pragma solidity >=0.5.5;

import "../token/ERC1400Raw/IERC1400TokensValidator.sol";

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";

import "../token/ERC1820/ERC1820Implementer.sol";

import "./../../AuthOracle/IAuthOracle.sol";

contract ERC1400BrregValidator is
    IERC1400TokensValidator,
    Ownable,
    ERC1820Implementer
{
    using SafeMath for uint256;

    IAuthOracle internal _authOracle;

    string
        internal constant ERC1400_TOKENS_VALIDATOR = "ERC1400TokensValidator";

    constructor(address authOracle) public {
        ERC1820Implementer._setInterface(ERC1400_TOKENS_VALIDATOR);
        _authOracle = IAuthOracle(authOracle);
    }

    function canValidate(
        bytes4 functionSig,
        bytes32 partition,
        address operator,
        address from,
        address to,
        uint256 value,
        bytes calldata data,
        bytes calldata operatorData // Comments to avoid compilation warnings for unused variables.
    ) external view returns (bool) {
        return (
            _canValidate(
                functionSig,
                partition,
                operator,
                from,
                to,
                value,
                data,
                operatorData
            )
        );
    }

    function tokensToValidate(
        bytes4 functionSig,
        bytes32 partition,
        address operator,
        address from,
        address to,
        uint256 value,
        bytes calldata data,
        bytes calldata operatorData // Comments to avoid compilation warnings for unused variables.
    ) external {
        require(
            _canValidate(
                functionSig,
                partition,
                operator,
                from,
                to,
                value,
                data,
                operatorData
            ),
            "A7"
        ); // Transfer Blocked - Identity restriction
    }

    function _canValidate(
        bytes4, /* functionSig */
        bytes32, /*partition*/
        address, /*operator*/
        address, /* from */
        address, /* to */
        uint256, /*value*/
        bytes memory, /* data */
        bytes memory // Comments to avoid compilation warnings for unused variables. /*operatorData*/
    ) internal pure returns (bool) {
        // if (!_authOracle.is_address_attested(to)) {
        //     return false;
        // }

        return true;
    }
}
