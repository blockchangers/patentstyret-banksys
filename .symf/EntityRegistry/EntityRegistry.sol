pragma solidity ^0.5.5;

import "@openzeppelin/contracts/ownership/Ownable.sol";

contract EntityRegistry is Ownable {
    // Yes, we know this can be brute forced (very easily) and we will not use this solution in production.
    mapping(bytes32 => address) internal _idToAddress;
    mapping(address => bytes32) internal _addressToId;
    mapping(bytes32 => bytes32) internal _idToName;
    mapping(address => bytes32) internal _addressToName;
    mapping(bytes32 => address) internal _nameToAddress;
    bytes32[] private _nameList;

    event newId(address indexed adr, bytes32 indexed id, bytes32 name);

    constructor() public {
        _transferOwnership(0x81a6Fe25534cb713DFDd75e5DD49bA5c02EDe148);
    }

    // Removed as NIPO owns id
    // function set_my_id(bytes32 id, bytes32 name) {
    //     _id_to_address(msg.sender, id, name);
    // }

    function set_id_to_address(
        bytes32 id,
        bytes32 name,
        address adr
    ) external {
        _id_to_address(adr, id, name);
    }

    function suggest_id_to_address(bytes32 id, bytes32 name) external {
        // Yes, this is only for testing enviroments
        address randomishAddress = address(
            uint160(
                uint256(
                    keccak256(abi.encodePacked(id, blockhash(block.number)))
                )
            )
        );
        _id_to_address(randomishAddress, id, name);
    }

    function _id_to_address(
        address adr,
        bytes32 id,
        bytes32 name
    ) internal {
        require(adr != address(0), "ADDRESS can not be empty");
        require(id != bytes32(0), "id can not be empty");
        require(name != bytes32(0), "NAME can not be empty");
        _idToAddress[id] = adr;
        _addressToId[adr] = id;
        _idToName[id] = name;
        _addressToName[adr] = name;
        _nameToAddress[name] = adr;
        _nameList.push(name);
        emit newId(adr, id, name);
    }

    function get_name_list() external view returns (bytes32[] memory names) {
        return _nameList;
    }

    function get_address_from_name(bytes32 name)
        external
        view
        returns (address adr)
    {
        return _nameToAddress[name];
    }

    function get_address_from_id(bytes32 id)
        external
        view
        returns (address adr)
    {
        return _idToAddress[id];
    }

    function get_id_from_address(address adr)
        external
        view
        returns (bytes32 id)
    {
        return _addressToId[adr];
    }

    function get_name_from_address(address adr)
        external
        view
        returns (bytes32 name)
    {
        return _addressToName[adr];
    }

    function get_name_from_id(bytes32 id) external view returns (bytes32 name) {
        return _idToName[id];
    }

    function address_owns_id(address adr, bytes32 id)
        external
        view
        returns (bool attested)
    {
        require(adr == _idToAddress[id], "msg.sender does not hold id.");
        return true;
    }

    function is_address_attested(address adr) external view returns (bool) {
        require(
            _addressToId[adr] != bytes32(0) &&
                _idToAddress[_addressToId[adr]] == adr,
            "msg.sender must not have empty id && id must be set to msg.sender"
        );
        return true;
    }
}
