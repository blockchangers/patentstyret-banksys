usePlugin("buidler-ethers-v5");
    usePlugin("@blockchangers/buidler-typechain");
    usePlugin("buidler-deploy");

    // BUIDLER CONFIG
// BUIDLER CONFIG SETUP

module.exports = {
  "networks": {
    "dev": {
      "url": "HTTP://127.0.0.1:8545"
    },
    "brreg": {
      "url": "https://u1qdua80h5:Er0LWdZuKqOza22YNQKhtdFCbqRzhzGCRhuZgrtHZ9s@u1txh1ent0-u1ieecy018-rpc.us1-azure.kaleido.io"
    }
  },
  "paths": {
    "artifacts": "./.symf/.cache/buidler/artifacts",
    "cache": "./.symf/.cache/buidler/cache",
    "sources": "./.symf/",
    "tests": "./.symf/tests",
    "deploy": "./.symf/.cache/buidler/deploy",
    "deployments": "./.symf/.cache/buidler/deployments"
  },
  "solc": {
    "optimizer": {
      "enabled": true,
      "runs": 50
    }
  },
  "typechain": {
    "outDir": "./src/symf/dependencies/typechain",
    "target": "ethers-v5"
  },
  "namedAccounts": {
    "deployer": {
      "default": 0
    }
  }
}
// BUIDLER CONFIG SETUP

 
        